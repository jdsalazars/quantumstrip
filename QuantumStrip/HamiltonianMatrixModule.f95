!
!   Purpose:
!       This subroutine creates an (2n+1)x(2n+1) hamiltonian matrix
!       The terms of the specific problem must be inserted here
!   Warning:
!       The Hamiltonian tensor goes from 1 to 2*n+1, so if you are
!       going to add an aditional term take care of this.
!
MODULE HamiltonianMatrixModule
    USE ParamModule
    USE PhysModule
    USE MathModule
    IMPLICIT NONE

    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: hamiltonian = 0    !Hamiltonian tensor
    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: kineticEnergy = 0  !Kinetic energy tensor, n^2/R^2*dnm
    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: diamagneticTensor = 0  !Diamagnetic tensor -n*gamma
    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: paramagneticTensor = 0  !Paramagnetic tensor gamma**2*Rc**2/4
    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: elecFieldTensor = 0
    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: iMassTensor = 0    !Tensor of the variable mass 1/m(phi)
    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: barrierTensor = 0    !Tensor of the variable mass 1/m(phi)
    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: vEffTensor = 0    !Tensor of the effective potential

    !Private variables
    REAL, PRIVATE :: ribRadius
    REAL, PRIVATE :: magneticField
    REAL, PRIVATE :: elecField
    REAL, PRIVATE :: thetaEField
    REAL, PRIVATE :: Xalma
    REAL, DIMENSION(Nheavi), PRIVATE :: Xalmb
    REAL, PRIVATE :: ma        !Effective mass of the electron (or hole) in the potential-well
    REAL, DIMENSION(Nheavi), PRIVATE :: invmb  !Inverse of the effective mass of the electron(or hole) out of the potential-well
    REAL, DIMENSION(Nheavi) :: vBarrier = 0.000           !Adimensional potential of the material RESPECT TO THE POTENTIAL OF MA
    REAL, DIMENSION(2*Nheavi), PRIVATE :: thetaBarrier        !Array with the angular positions of the barriers

    !Private procedures
    PRIVATE :: fillKineticEnergy, fillDiamagneticTensor, fillParamagneticTensor, heightContribution
    PRIVATE :: fillInvMassTensor, fillElecFieldTensor, fillBarrierTensor, fillVEffTensor, vEffFunction

CONTAINS

    !This subroutine is used to generate the whole hamiltonian matrix,
    !It has a really high computational cost so it should only be used at the beginning
    !Of the program
    SUBROUTINE generateHamiltonianMatrix()

        hamiltonian = 0

        IF((Nheavi >= 1) .AND. (Xalmb(1) .NE. Xalma)) THEN
            CALL fillInvMassTensor()
            CALL fillBarrierTensor()
            hamiltonian = hamiltonian + barrierTensor
        END IF

        CALL fillKineticEnergy()
        CALL fillDiamagneticTensor()
        CALL fillParamagneticTensor()
        CALL fillElecFieldTensor()
        CALL fillVEffTensor()

        hamiltonian = hamiltonian+ kineticEnergy + diamagneticTensor + paramagneticTensor &
            + elecFieldTensor + vEffTensor

        !WRITE(*,1000) ((vEffTensor(i,j), j=1, 5), i=1, 5)
!1000    FORMAT(' ', 1X, 10F30.23)

    END SUBROUTINE generateHamiltonianMatrix
    !----------------------------------------------------------------
    !This subroutine is in charge of making each variable equal to its corresponding par
    !For example elecField = elecFieldPar
    SUBROUTINE restartParameters()

        INTEGER :: i

        ribRadius = ribRadiusPar
        magneticField = magneticFieldPar
        elecField = elecFieldPar
        thetaEField = pi*thetaEFieldPar/180.0
        Xalma = XalmaPar
        Xalmb = XalmbPar
        thetaBarrier = pi*thetaBarrierPar/180.0
        !Decides if using the mass of an electron or a hole
        IF(PARTICLE == "E") THEN
            ma = EFECMASS(Xalma,Pman,Temp)
            DO i =1, Nheavi
                invmb(i) = 1.0/EFECMASS(Xalmb(i),Pman,Temp)
            END DO
        ELSE IF(PARTICLE == "H") THEN
            ma = EFECHOLEMASS(Xalma)
            DO i =1, Nheavi
                invmb(i) = 1.0/EFECMASS(Xalmb(i),Pman,Temp)
            END DO
        END IF

    END SUBROUTINE
    !----------------------------------------------------------------
    !This subroutine fills the kinetic energy tensor to the hamiltonian matrix
    SUBROUTINE fillKineticEnergy()

        INTEGER :: i, j    !Counter

        !If there are azimutal barriers
        IF((Nheavi >= 1) .AND. (Xalmb(1) .NE. Xalma)) THEN
            DO i = -ADIM, ADIM
                DO j = -ADIM, ADIM
                    kineticEnergy(i+ADIM+1, j+ADIM+1) = i*j*iMassTensor(i+ADIM+1, j+ADIM+1)/ribRadius**2
                END DO
            END DO
        ELSE
            DO i=-ADIM,ADIM
                kineticEnergy(i+ADIM+1,i+ADIM+1) = (i/ribRadius)**2
            END DO
        END IF

        !WRITE(*,1000) ((iMassTensor(i,j), j=1, 2*ADIM+1), i = 1, 2*ADIM+1)
!1000    FORMAT(' ', 22F30.17)

    END SUBROUTINE fillKineticEnergy
    !----------------------------------------------------------------
    !This subroutine fills the diamagnetic tensor <m|-n*gamma |n> which will be added in the hamiltonian matrix
    SUBROUTINE fillDiamagneticTensor()

        INTEGER :: i, j    !Counter

        diamagneticTensor = 0

        IF((Nheavi >= 1) .AND. (Xalmb(1) .NE. Xalma)) THEN
            DO i = -ADIM, ADIM
                DO j = -ADIM, ADIM
                    diamagneticTensor(i+ADIM+1,j+ADIM+1) = - (i+j)*iMassTensor(i+ADIM+1,j+ADIM+1)*magneticField/2
                END DO
            END DO
        ELSE
            DO i=-ADIM,ADIM
                diamagneticTensor(i+ADIM+1,i+ADIM+1) = - i*magneticField
            END DO
        END IF

    END SUBROUTINE fillDiamagneticTensor
    !----------------------------------------------------------------
    !This subroutine fills the paramagnetic tensor <m|gamma**2*Rc**2/4|n> which will be added in the hamiltonian matrix
    SUBROUTINE fillParamagneticTensor()

        INTEGER :: i    !Counter

        paramagneticTensor = 0

        IF((Nheavi >= 1) .AND. (Xalmb(1) .NE. Xalma)) THEN
            paramagneticTensor = iMassTensor*(magneticField*ribRadius/2)**2
        ELSE
            DO i=-ADIM,ADIM
                paramagneticTensor(i+ADIM+1,i+ADIM+1) = (magneticField*ribRadius/2.0)**2
            END DO
        END IF

    END SUBROUTINE fillParamagneticTensor
    !----------------------------------------------------------------
    !This subroutine fills the electric field tensor
    SUBROUTINE fillElecFieldTensor()

        INTEGER :: i    !Counter

        !Generate the electric field tensor
        elecFieldTensor(2*ADIM+1, 2*ADIM) = 0.5*ribRadius*elecField*EXP(CMPLX(0, -thetaEField))
        elecFieldTensor(1, 2) = 0.5*ribRadius*elecField*EXP(CMPLX(0, thetaEField))
        DO i = -ADIM+1, ADIM-1
            elecFieldTensor(i+ADIM+1, i+ADIM) = 0.5*ribRadius*elecField*ZEXP(DCMPLX(0, -thetaEField))
            elecFieldTensor(i+ADIM+1, i+ADIM+2) = 0.5*ribRadius*elecField*ZEXP(DCMPLX(0, thetaEField))
        END DO


    END SUBROUTINE
    !----------------------------------------------------------------
    !This subroutine fills the inverse mass tensor <m| 1/m(phi) |n>
    SUBROUTINE fillInvMassTensor()

        iMassTensor = heaviside(Nheavi, ma*invmb, 1.0, thetaBarrier)

    END SUBROUTINE fillInvMassTensor
    !----------------------------------------------------------------
    !This subroutine fills the inverse mass tensor <m| 1/m(phi) |n>
    SUBROUTINE fillBarrierTensor()

        INTEGER :: i
        !Gets the values of the potencial barriers
        IF (PARTICLE == "E") THEN
            DO i=1, Nheavi
                vBarrier(i)=(EGAP(Xalmb(i),Pman,Temp)-EGAP(Xalma,Pman,Temp))*(2.0/3.0)/REAL(Rydberg)
            END DO
        ELSE IF (PARTICLE == "H") THEN
            DO i=1, Nheavi
                vBarrier(i)=(EGAP(Xalmb(i),Pman,Temp)-EGAP(Xalma,Pman,Temp))*(1.0/3.0)/REAL(Rydberg)
            END DO
        ENDIF
        barrierTensor = heaviside(Nheavi, vBarrier, 0.0000, thetaBarrier)

    END SUBROUTINE fillBarrierTensor
    !----------------------------------------------------------------
    !This subroutine fills the effective potential tensor...
    !The effective potential tensor contains all of the terms in the hamiltonian
    !Which its integral must be found numerically
    SUBROUTINE fillVEffTensor()

        INTEGER :: n, m

        DO m = -ADIM, ADIM
            DO n = m, ADIM

                CALL intTrap(vEffFunction, DBLE(0.0), DBLE(2*pi), vEffTensor(m+ADIM+1, n+ADIM+1), m, n)

            END DO

        END DO

    END SUBROUTINE fillVEffTensor
    !----------------------------------------------------------------
    !The following function is used to fill the effective potential tensor
    FUNCTION vEffFunction(phi, m, n)

        COMPLEX*16 :: vEffFunction
        INTEGER :: m, n     !Index of the bra and ket respectively <m|Veff|n>
        DOUBLE PRECISION :: phi!, phiTemp

        vEffFunction = heightContribution(phi)

        IF(n .NE. m) THEN
            !phiTemp = phi - INT(phi/2*pi)*2*pi
            vEffFunction = vEffFunction*EXP(DCMPLX(0.0, phi*(n-m)))
        END IF
        vEffFunction = vEffFunction/(2*pi)

    END FUNCTION vEffFunction
    !The following function is the contribution of the height to the hamiltonian
    !There are two terms pi**2/(m(phi)*h(phi)**2) and (d(Ln(h(phi)))/dphi)**2
    FUNCTION heightContribution(phi)

        DOUBLE PRECISION :: heightContribution
        DOUBLE PRECISION, INTENT(IN) :: phi
        DOUBLE PRECISION :: inverseMass

        IF((deltaPar == 0.0) .OR. (numRos == 0)) THEN
            heightContribution = (pi/hrib)**2
        ELSE
            heightContribution = (pi/hrib)**2/(1-deltaPar*(1-COS(numRos*phi))) &
                + ((4*pi**2+3.0)/(12.0*ribRadius**2))*(0.5*deltaPar*numRos*DSIN(numRos*phi)/(1-deltaPar*(1-DCOS(numRos*phi))))**2
        END IF

        inverseMass = inverseMassFunction(phi)
        heightContribution = inverseMass*heightContribution

    END FUNCTION heightContribution

    !The following function is the piece wise function for the mass. Which is generally
    !of the shape of a heaviside function.
    FUNCTION inverseMassFunction(phi)

        DOUBLE PRECISION :: inverseMassFunction
        DOUBLE PRECISION, INTENT(IN) :: phi
        INTEGER :: i
        !DOUBLE PRECISION :: phiTemp

        inverseMassFunction = 1

        Hamiltonian = Hamiltonian + (newMagneticField-magneticField)*MATMUL(iMassTensor,diamagneticTensor) &
            + (newMagneticField**2-magneticField**2)*MATMUL(iMassTensor,paramagneticTensor)
        !heightMassFunction = 0

        IF (Nheavi > 0) THEN
            IF (phi < thetaBarrier(1) .OR. phi > thetaBarrier(2*Nheavi)) inverseMassFunction = 1

            IF (phi > thetaBarrier(2*Nheavi-1) .AND. phi < thetaBarrier(2*Nheavi)) inverseMassFunction = ma*invmb(Nheavi)

            DO i = 1,Nheavi-1
                IF (phi > thetaBarrier(2*i-1) .AND. phi < thetaBarrier(2*i)) inverseMassFunction = ma*invmb(i)

                IF (phi > thetaBarrier(2*i) .AND. phi < thetaBarrier(2*i+1)) inverseMassFunction = 1
            ENDDO
        ENDIF

    END FUNCTION inverseMassFunction

    !================================================================
    !From here the subroutines are changers used to change the parameters and reconstruct
    !The hamiltonina matrix
    !----------------------------------------------------------------
    SUBROUTINE changeMagneticField(newMagneticField)

        REAL, INTENT(IN) :: newMagneticField
        !Subtract the old diamagnetic and paramagnetic tensors
        hamiltonian = hamiltonian - (diamagneticTensor+paramagneticTensor)
        !Recalculate the diamagnetic and paramagnetic tensors
        magneticField = newMagneticField
        CALL fillDiamagneticTensor()
        CALL fillParamagneticTensor()
        hamiltonian = hamiltonian + diamagneticTensor+paramagneticTensor

    END SUBROUTINE
    !----------------------------------------------------------------
    SUBROUTINE changeElectricField(newElecField)

        REAL, INTENT(IN) :: newElecField

        !Subtract the old diamagnetic and paramagnetic tensors
        hamiltonian = hamiltonian - elecFieldTensor
        !Recalculate the diamagnetic and paramagnetic tensors
        elecField = newElecField
        CALL fillElecFieldTensor()
        hamiltonian = hamiltonian + elecFieldTensor

    END SUBROUTINE

END MODULE HamiltonianMatrixModule
