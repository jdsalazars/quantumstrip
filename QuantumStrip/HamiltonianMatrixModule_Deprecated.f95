!
!   Purpose:
!       This subroutine creates an (2n+1)x(2n+1) hamiltonian matrix
!       The terms of the specific problem must be inserted here
!   Warning:
!       The Hamiltonian tensor goes from 1 to 2*n+1, so if you are
!       going to add an aditional term take care with this.
!
!MODULE HamiltonianMatrixModule_Deprecated

    !USE ParamModule
    !USE MathModule
    !USE PhysModule

!    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: Hamiltonian   !Hamiltonian matrix
!    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: iMassTensor  !Inverse-Mass Tensor for this problem
!    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: VTensor      !Potential barrier tensor
!    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: HTensor      !Height tensor
!    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: EffectiveVTensor      !Height tensor
!    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: ElecFieldTensor      !Electric field tensor
!    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: diamagneticTensor     !diamagnetic field tensor
!    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: paramagneticTensor     !paramagnetic field tensor
!
!    CONTAINS
!
!    SUBROUTINE HamiltonianMatrix(Hamiltonian, n)
!        IMPLICIT NONE
!
!        !---------------------------------------------------------
!        !Counters
!        INTEGER :: i,j,k
!        !Variables
!        INTEGER, INTENT(IN) :: n
!        COMPLEX*16, DIMENSION(2*n+1, 2*n+1), INTENT(OUT) :: Hamiltonian !Hamiltonian matrix
!        !---------------------------------------------------------
!        !Starts the program
!        !Initialize the arrays
!        Hamiltonian = 0
!        iMassTensor = 0
!        VTensor = 0
!        HTensor = 0
!        EffectiveVTensor = 0
!        ElecFieldTensor = 0
!
!        !Gets the values of the masses ma, mb, mha and mhb
!        ma = EFECMASS(Xalma,Pman,Temp)
!        mha = EFECHOLEMASS(Xalma)
!
!        DO i =1, Nheavi
!            invmb(i) = 1.0/EFECMASS(Xalmb(i),Pman,Temp)
!            invmhb(i) = 1.0/EFECHOLEMASS(Xalmb(i))
!        END DO
!
!        !Gets the values of the potencial barriers
!        DO i=1, Nheavi
!            IF (PARTICLE == "E") THEN
!                Vo(i)=(EGAP(Xalmb(i),Pman,Temp)-EGAP(Xalma,Pman,Temp))*(2.0/3.0)/Rydberg
!            ELSE IF (PARTICLE == "H") THEN
!                Vo(i)=(EGAP(Xalmb(i),Pman,Temp)-EGAP(Xalma,Pman,Temp))*(1.0/3.0)/Rydberg
!            ENDIF
!        ENDDO
!
!        !Vo = 1000000.0
!
!        !invmb = 10000000.0
!        !ma = 10000000.0
!
!        !Generate the inverse mass tensor with the heaviside function
!        IF (PARTICLE == "E") THEN
!            iMassTensor = heaviside(Nheavi, ma*invmb, 1.0, theta)
!        ELSE IF (PARTICLE == "H") THEN
!            iMassTensor = heaviside(Nheavi, ma*invmhb, ma/mha, theta)
!        ENDIF
!
!        !Generate the effective potencial tensor with the effectivePotencialTensor function
!        EffectiveVTensor = effectivePotencialTensor()
!
!        !Generate the Potential tensor with the heaviside function and adds it to the
!        !Hamiltonian tensor
!        VTensor = heaviside(Nheavi, Vo, 0.0, theta)
!
!        !Generate the electric field tensor
!        ElecFieldTensor(2*n+1, 2*n) = 0.5*Rc*EXP(CMPLX(0, -thetaEField))
!        ElecFieldTensor(1, 2) = 0.5*Rc*EXP(CMPLX(0, thetaEField))
!        DO i = -n+1, n-1
!            ElecFieldTensor(i+n+1, i+n) = 0.5*Rc*EXP(CMPLX(0, -thetaEField))
!            ElecFieldTensor(i+n+1, i+n+2) = 0.5*Rc*EXP(CMPLX(0, thetaEField))
!        END DO
!
!        IF (PARTICLE == "H") ElecFieldTensor = -ElecFieldTensor
!
!        !Creates the diamagnetic and paramagnetic tensors
!        diamagneticTensor = 0
!        paramagneticTensor = 0
!        DO i = -n, n
!                diamagneticTensor(i+n+1,i+n+1) = -i
!                paramagneticTensor(i+n+1,i+n+1) = 0.25*Rc**2
!        ENDDO
!
!        !Add the columns and rows of the Hamiltonian matrix
!        hamr: DO i = -n, n
!            hamc: DO j=i,n
!            !The terms added here are the following in that order
!            !1. The kinetic energy term
!            !2. The paramagnetic term
!            !3. The diamagnetic term
!                Hamiltonian(i+n+1, j+n+1) = (j**2/Rc**2)*iMassTensor(i+n+1,j+n+1)+Hamiltonian(i+n+1, j+n+1)
!
!            END DO hamc
!        END DO hamr
!
!        Hamiltonian = Hamiltonian + magneticField*MATMUL(iMassTensor,diamagneticTensor) &
!            +magneticField**2*MATMUL(iMassTensor,paramagneticTensor)
!
!        Hamiltonian = Hamiltonian + VTensor + EffectiveVTensor + electricField*ElecFieldTensor + AcceptorTensor
!
!    END SUBROUTINE HamiltonianMatrix
!
!    !The following procedure changes the magnetic field terms to the hamiltonian matrix
!    SUBROUTINE changeMagneticField(Hamiltonian,newMagneticField)
!        COMPLEX*16,dimension(2*ADIM+1,2*ADIM+1), INTENT(INOUT) :: Hamiltonian
!        REAL, INTENT(IN) :: newMagneticField
!
!        !Removes the antique values of the diamagnetic and paramagnetic terms and add the new ones
!
!        Hamiltonian = Hamiltonian + (newMagneticField-magneticField)*MATMUL(diamagneticTensor,iMassTensor) &
!            + (newMagneticField**2-magneticField**2)*MATMUL(paramagneticTensor,iMassTensor)
!
!        magneticField = newMagneticField
!
!    END SUBROUTINE changeMagneticField
!
!    !The following procedure changes the magnetic field terms to the hamiltonian matrix
!    SUBROUTINE changeElectricField(Hamiltonian,newElectricField)
!        COMPLEX*16,DIMENSION(2*ADIM+1,2*ADIM+1), INTENT(INOUT) :: Hamiltonian
!        REAL, INTENT(IN) :: newElectricField
!        !Removes the antique values of the diamagnetic and paramagnetic terms and add the new ones
!        Hamiltonian = Hamiltonian + (newElectricField-electricField)*elecFieldTensor
!
!        electricField = newElectricField
!
!    END SUBROUTINE changeElectricField

!END MODULE HamiltonianMatrixModule_Deprecated
