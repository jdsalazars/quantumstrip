MODULE IteratorsModule
    USE HamiltonianMatrixModule
    USE PrinterModule
    USE timerdata
    IMPLICIT NONE

    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: hamiltonianCopy = 0    !A copy of the hamiltonian tensor

    !The following are parameters defined to use the ZHEEV subroutine
    DOUBLE PRECISION, DIMENSION(2*ADIM+1), PRIVATE :: eigenCopy      !Vector of eigenvalues (Ascending order)
    INTEGER, PARAMETER, PRIVATE :: LWORK = 4*ADIM+1
    COMPLEX*16, DIMENSION(LWORK), PRIVATE :: WORK            !Output array needed to compute eigenvalues
    DOUBLE PRECISION, DIMENSION(6*ADIM+1), PRIVATE :: RWORK  !Output array needed to compute eigenvalues
    INTEGER, PRIVATE :: INFO                                 !Problem report integer
    !Private variables
    CHARACTER(len=20), PRIVATE :: filename                   !Head of the filename where everything will be printed
    CHARACTER(len=20) :: filenametemp               !Variable used to give the name to each file
    !Private procedures
    PRIVATE :: iterateChanger


CONTAINS

    !This subroutine changes the value of a parameter in the hamiltonian matrix
    !Using the appropiated subroutine defined in the HamiltonianMatrixModule
    SUBROUTINE iterateChanger(changerSubroutine, start, end, nIntervals, filename)

        EXTERNAL :: changerSubroutine
        REAL, INTENT(IN) :: start, end  !Start and end of the interval
        INTEGER, INTENT(IN) :: nIntervals   !Number of divisions of the interval
        DOUBLE PRECISION, DIMENSION(nIntervals, 2*ADIM+2) :: eigenVal  !Matrix which contains the eigenvalues
        CHARACTER(len = 20), INTENT(IN) :: filename !Name of the file where will be printed the eigenvalues
        INTEGER :: i    !Counter
        REAL :: parameter       !Parameter that will change

        !Restart the whole parameters of the hamiltonian matrix
        CALL restartParameters()

        DO i = 1, nIntervals

            parameter = start + (i-1.0)*(end-start)/(nIntervals-1.0)
            eigenVal(i, 1) = parameter
            CALL changerSubroutine(parameter)
            hamiltonianCopy = hamiltonian
            CALL ZHEEV("N", "U", 2*ADIM+1, hamiltonianCopy, 2*ADIM+1, eigenCopy, WORK, LWORK, RWORK, INFO)
            eigenVal(i, 2:2*ADIM+2) = eigenCopy(1:2*ADIM+1)

        END DO

        CALL MatrixWriter(eigenVal, nIntervals, 2*ADIM+2, filename)

    END SUBROUTINE iterateChanger
    !----------------------------------------------------------------
    !This subroutine iterates over all of the possible properties (The ones with propertynum .NE. 1)
    !and prints its respective curve in a file
    SUBROUTINE changeAllProperties()

        !Asks for the name of the files to be created
        WRITE(*,100)
100     FORMAT(' ', 'Please insert the filename to create')
        READ(*,*) filename

        !Here go all of the possible changers of the HamiltonianMatrixModule
        !Variable magnetic field
        IF(gnum .NE. 0) THEN
            filenametemp = trim(filename)//"EigenAB.dat"
            CALL iterateChanger(changeMagneticField, gini, gend, gnum, filenametemp)
        END IF
        !Variable electric field
        IF(EFnum .NE. 0) THEN
            filenametemp = trim(filename)//"EigenEF.dat"
            CALL iterateChanger(changeElectricField, EFini, EFend, EFnum, filenametemp)
        END IF

    END SUBROUTINE
    !----------------------------------------------------------------
    !This subroutine creates a document with the eigenfunctions coefficients and another one
    !With the data needed to graph.
    SUBROUTINE generateEigenFunction()

        REAL, DIMENSION(4, 2*Nheavi) :: data       !Matrix which contains data to be printed

        !Restart the whole parameters of the hamiltonian matrix
        CALL restartParameters()
        CALL generateHamiltonianMatrix()
        hamiltonianCopy = hamiltonian
        CALL ZHEEV("V", "U", 2*ADIM+1, hamiltonianCopy, 2*ADIM+1, eigenCopy, WORK, LWORK, RWORK, INFO)
        !Fills the array Dat which contains information about the system
        !-----------------------------------------------------
        !The matrix that contains the data to be printed is filled
        data = 0.0
        data(1,1) = Nheavi
        data(2,1) = ribRadiusPar
        data(3,:) = thetaBarrierPar*pi/180
        data(4,1:Nheavi) = vBarrier
        filenametemp = trim(filename)//"EigenVec.dat"
        CALL MatrixWriter(hamiltonianCopy, 2*ADIM+1, 2*ADIM+1, filenametemp)
        filenametemp = trim(filename)//"Datos.dat"
        CALL MatrixWriter(data, 4, 2*Nheavi, filenametemp)

    END SUBROUTINE


END MODULE IteratorsModule
