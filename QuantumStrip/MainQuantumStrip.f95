!   Purpose:
!       This program calculates the eigenvalues and eigenvectors
!       of an specific matrix A using the ZHEEV subroutine of the
!       LAPACK libraries for hermitian matrixes. The eigenvalues are printed
!       in ascending order.
!
!   Warnings:
!       Any of the parameters in this program must be changed, the definition
!       of the matrix and its parameters can be changed in the module
!       "ModFunYSubs.f95"
!
!   Record of revisions:
!       Date            Programmer
!       ====           ============
!    29/06/2016         D. Fonnegra
!
PROGRAM MainQuantumStrip
    USE HamiltonianMatrixModule
    USE IteratorsModule
    IMPLICIT NONE

    !Subroutine used to compute the time that lasts the program executing
    CALL time_marker

    CALL initialConfiguration()
    CALL generateHamiltonianMatrix()
    CALL changeAllProperties()
    CALL generateEigenFunction()

    CALL time_marker
    CALL time_elapsed

END PROGRAM MainQuantumStrip

SUBROUTINE initialConfiguration()
    USE ParamModule
    USE PhysModule
    USE HamiltonianMatrixModule
    !Some constants are defined to give dimension to the variables
    !Rydberg in meV
    !Borh's radius in meters
    epsil=4*pi*DIELEC(Pman,Temp)
    Rydberg=e**4.0*EFECMASS(Xalma,Pman,Temp)*me/(2*hbar**2*(epsil*eo)**2)
    Rydberg=Rydberg*6.2415093433E+21
    a0=hbar**2*epsil*eo/(EFECMASS(Xalma,Pman,Temp)*me*e**2)
    CALL restartParameters()
END SUBROUTINE initialConfiguration
