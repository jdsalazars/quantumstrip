!PROGRAM MainQuantumStrip_Deprecated
!!
!!   Purpose:
!!       This program calculates the eigenvalues and eigenvectors
!!       of an specific matrix A using the ZHEEV subroutine of the
!!       LAPACK libraries for hermitian matrixes. The eigenvalues are printed
!!       in ascending order.
!!
!!   Warnings:
!!       Any of the parameters in this program must be changed, the definition
!!       of the matrix and its parameters can be changed in the module
!!       "ModFunYSubs.f95"
!!
!!   Record of revisions:
!!       Date            Programmer
!!       ====           ============
!!    29/06/2016         D. Fonnegra
!!
!USE HamiltonianMatrixModule
!USE ParamModule
!USE Measurements
!USE PhysModule
!USE PrinterModule
!USE timerdata
!USE MathModule
!USE OpticalProperties
!USE ProbabilityDensity
!IMPLICIT NONE
!
!    !----------------------------------------------------
!    !Measured observables
!    REAL :: Lz                                      !Z-component of the angular momentum
!    !External subroutines
!    EXTERNAL ZGEEV!, HamiltonianMatrix
!    !Parameters
!    INTEGER, PARAMETER :: LWORK = 4*ADIM+1
!    INTEGER, PARAMETER :: Ndat = 6
!    !Program arrays and variables
!    CHARACTER(len=20) :: filename                   !Name of the file to print the eigenvalues
!    CHARACTER(len=20) :: filenametemp               !Variable used to give the name to each file
!    COMPLEX*16, DIMENSION(2*ADIM+1,2*ADIM+1) :: A   !Matrix to be diagonalized
!    DOUBLE PRECISION, DIMENSION(2*ADIM+1) :: W      !Vector of eigenvalues (Ascending order)
!    DOUBLE PRECISION, DIMENSION(gnum, 2*ADIM+2) :: EigenE_AB !Matrix of eigenvalues of energy for the AB oscillations
!    DOUBLE PRECISION, DIMENSION(EFnum, 2*ADIM+2) :: EigenE_EF !Matrix of eigenvalues of energy as a function of the electric field
!    DOUBLE PRECISION, DIMENSION(Alphanum, 2*ADIM+2) :: EigenE_Alpha !Matrix of eigenvalues of energy as a function of the angle alpha
!    DOUBLE PRECISION, DIMENSION(Rcnum, 2*ADIM+2) :: EigenE_Rc !Matrix of eigenvalues of energy as a function of the radius
!    DOUBLE PRECISION, DIMENSION(RDonornum, 2*ADIM+2) :: EigenE_RDonor !Matrix of eigenvalues of energy as a function of the position of the donor
!    DOUBLE PRECISION, DIMENSION(deltanum, 2*ADIM+2) :: EigenE_delta !Matrix of the eigenvalues of the energy as a function of delta
!    DOUBLE PRECISION, DIMENSION(VoAccenum, 2*ADIM+2) :: EigenE_VoAcceptor !Matrix of the eigenvalues of the energy as a function of VoAcceptor
!    DOUBLE PRECISION, DIMENSION(RoAccenum, 2*ADIM+2) :: EigenE_RoAcceptor !Matrix of the eigenvalues of the energy as a function of RoAcceptor
!    DOUBLE PRECISION, DIMENSION(numPoints, 2) :: DensiProbMatrix !Matrix of the eigenvalues of the energy as a function of RoAcceptor
!    DOUBLE PRECISION, DIMENSION(500, 7) :: EffectiveAcceptorPotentialGraph !Matrix of the effective accpetor potencial
!    DOUBLE PRECISION, DIMENSION(hwnum, 7) :: OpticalProp !Matrix of the values of the optical properties
!    DOUBLE PRECISION, DIMENSION(hwnum, 5) :: OpticalProp2 !Matrix of the values of the optical properties
!    REAL, DIMENSION(Ndat, 2*Nheavi) :: Dat          !Matrix which contains data to be printed
!    COMPLEX*16, DIMENSION(2*ADIM+1,2*ADIM+1) :: EigenV    !Matrix of eigenvectors
!    COMPLEX*16, DIMENSION(LWORK) :: WORK            !Output array needed to compute eigenvalues
!    DOUBLE PRECISION, DIMENSION(6*ADIM+1) :: RWORK  !Output array needed to compute eigenvalues
!    INTEGER :: INFO                                 !Problem report integer
!    DOUBLE PRECISION :: LIndex          !Linear refractive index change
!    DOUBLE PRECISION :: NLIndex         !Non-Linear refractive index change
!    DOUBLE PRECISION :: LAbs            !Linear absorbtion coefficient
!    DOUBLE PRECISION :: NLAbs           !Non-Linear absorbtion coefficient
!
!    !Counters
!    INTEGER :: alpc, i, j
!
!    !----------------------------------------------------
!    !Starts the program
!    CALL time_marker
!
!    CALL initialConfiguration()
!    !Initialize the arrays
!    EigenE_AB = 0
!    EigenE_EF = 0
!    EigenE_Rc = 0
!    OpticalProp = 0
!    W = 0
!
!    !----------------------------------------------------
!    !Generates the Hamiltonian matrix
!    !Calls the ZHEEV subroutine which computes the eigenvalues of an hermitian matrix
!    !theta = (thetaPar)*pi/180
!    !thetaEField = thetaEField*pi/180
!    !magneticField = gpar
!    !electricField = electricFieldPar
!    !Rc = Rcpar
!    IF (NDonors==1)RDonor = RoDonors(1)
!    PhiDonors = (PhiDonors)*pi/180
!    deltha = deltapar
!    !Generates the Acceptor Tensor
!!    DO i=1, numAccePot+1
!!        AcceptorPotentialArray(i,1) = (i-1)*2*pi/(numAccePot)
!!        AcceptorPotentialArray(i,2) = ZAverage(AcceptorPotencial,DBLE((i-1)*2*pi/(numAccePot)))
!!    ENDDO
!!    RoAcceptor = RoAcceptorPar
!!    VoAcceptor = VoAcceptorPar
!!    AcceptorTensor = 0
!!    IF (VoAcceptorPar .NE. 0 .OR. VoAccenum > 1) THEN
!!        AuxAcceptorTensor = effectiveAcceptorPotencialTensor()
!!        AcceptorTensor = VoAcceptor*AuxAcceptorTensor
!!    ENDIF
!!    WRITE(*,*)AcceptorTensor
!!    READ(*,*)Lz
!
!    !----------------------------------------------------
!    CALL HamiltonianMatrix(A, ADIM)
!    CALL ZHEEV("V", "U", 2*ADIM+1, A, 2*ADIM+1, W, WORK, LWORK, RWORK, INFO)
!    EigenE_AB(1,1) = magneticField
!    EigenE_EF(1,1) = electricField
!    EigenE_AB(1, 2:2*ADIM+2) = W(1:2*ADIM+1)
!    EigenE_EF(1, 2:2*ADIM+2) = W(1:2*ADIM+1)
!    EigenV = A
!
!    !The following lines draw the probability density of the state
!    !-----------------------------------------------------
!    DensiProbMatrix = 0
!    DensiProbMatrix = DensiProb(A,2)
!
!    !The following lines get the Optical properties of the system
!    !-----------------------------------------------------
!    hwif: IF (hwnum>1) THEN
!
!            W = W*Rydberg*e/1000
!

!      hwdo: DO alpc = 1, hwnum
!
!                hw = hwini+(alpc-1.0)*(hwend-hwini)/(hwnum-1.0)
!                OpticalProp(alpc,1) = hw*Rydberg/1000
!                OpticalProp2(alpc,1) = hw
!                hw = hw*Rydberg*e/1000
!
!                CALL OptProp(hw,A,W,LIndex,NLIndex,LAbs,NLAbs,2,1)
!
!                OpticalProp(alpc, 2:7) = (/LIndex,NLIndex,LIndex+NLIndex,LAbs,NLAbs,LAbs+NLAbs/)
!
!                !----------
!                !this is just to check the values os the optprop
!!                OpticalProp2(alpc, 2:5) = (/linearindex(hw,a,w),nonlinearindex(hw,a,w), &
!!                                          LinearAbs(hw,A,W),NonLinearAbs(hw,A,W)/)
!
!            END DO hwdo
!    END IF hwif
!
!    !Auxiliar matrix to do the curves
!    Hamiltonian = 0
!    CALL HamiltonianMatrix(Hamiltonian, ADIM)
!
!    !The following lines get the eigenvalues of the energy for a variable magnetic field
!    !-----------------------------------------------------
!    magneticField = gpar
!    ABif: IF (gnum>1) THEN
!        W = 0.0
!        ABdo: DO alpc = 1, gnum
!
!            magneticField = gini+(alpc-1.0)*(gend-gini)/(gnum-1.0)
!            EigenE_AB(alpc,1) = magneticField
!            CALL changeMagneticField(Hamiltonian,magneticField)
!            A = Hamiltonian
!            CALL ZHEEV("N", "U", 2*ADIM+1, A, 2*ADIM+1, W, WORK, LWORK, RWORK, INFO)
!            EigenE_AB(alpc, 2:2*ADIM+2) = W(1:2*ADIM+1)
!
!        END DO ABdo
!    END IF ABif
!
!    !The following lines get the eigenvalues of the energy for a variable electric field
!    !-----------------------------------------------------
!    electricField = electricFieldpar
!    EFif: IF (EFnum>1) THEN
!        W = 0.0
!        EFdo: DO alpc = 1, EFnum
!
!            electricField = EFini+(alpc-1.0)*(EFend-EFini)/(EFnum-1.0)
!            EigenE_EF(alpc,1) = electricField
!            CALL changeElectricField(Hamiltonian,electricField)
!            A = Hamiltonian
!            CALL ZHEEV("N", "U", 2*ADIM+1, A, 2*ADIM+1, W, WORK, LWORK, RWORK, INFO)
!            EigenE_EF(alpc, 2:2*ADIM+2) = W(1:2*ADIM+1)
!
!        END DO EFdo
!    END IF EFif
!
!    !The following lines get the eigenvalues of the energy for a variable angle between the barriers
!    !-----------------------------------------------------
!    Alphaini = (Alphaini)*pi/180
!    Alphaend = (Alphaend)*pi/180
!
!    Alphaif: IF (Alphanum>1) THEN
!        IF (SIZE(theta)==4) THEN
!            W = 0.0
!            Alphado: DO alpc = 1, Alphanum
!
!                Alpha = Alphaini+(alpc-1.0)*(Alphaend-Alphaini)/(Alphanum-1.0)
!                theta(1) = Alpha/2.0
!                theta(2) = theta(1)+betha(1)*pi/180
!                theta(3) = 2*pi-betha(2)*pi/180-Alpha/2.0
!                theta(4) = 2*pi-Alpha/2.0
!                !thetaEField = (Alpha/2.0 + 180)*pi/180
!                !theta = (/Alpha/2.0, Alpha/2.0+, Alpha + betha(1)*pi/180, Alpha + betha(1)*pi/180 + betha(2)*pi/180/)
!                EigenE_Alpha(alpc,1) = Alpha*180/pi
!                CALL HamiltonianMatrix(A,ADIM)
!                CALL ZHEEV("N", "U", 2*ADIM+1, A, 2*ADIM+1, W, WORK, LWORK, RWORK, INFO)
!                EigenE_Alpha(alpc, 2:2*ADIM+2) = W(1:2*ADIM+1)
!
!            END DO Alphado
!
!        ELSE
!
!            WRITE(*,*) "Cannot get the alpha curves because the size of theta is not 4"
!
!        END IF
!
!    END IF Alphaif
!
!!    !The following lines get the eigenvalues of the energy for a variable radius
!!    !-----------------------------------------------------
!!    Rcif: IF (Rcnum>1) THEN
!!        W = 0.0
!!        Rcdo: DO alpc = 1, Rcnum
!!            Rc = Rcini+(alpc-1.0)*(Rcend-Rcini)/(Rcnum-1.0)
!!!            RoAcceptor = Rc
!!!            dAcceptor = 2*Rc
!!            IF (VoAcceptorPar .NE. 0) THEN
!!                AcceptorTensor = VoAcceptorPar*effectiveAcceptorPotencialTensor()
!!            ENDIF
!!            EigenE_Rc(alpc,1) = Rc
!!            CALL HamiltonianMatrix(A,ADIM)
!!            CALL ZHEEV("N", "U", 2*ADIM+1, A, 2*ADIM+1, W, WORK, LWORK, RWORK, INFO)
!!            EigenE_Rc(alpc, 2:2*ADIM+2) = W(1:2*ADIM+1)
!!
!!        END DO Rcdo
!!    END IF Rcif
!
!    !The following lines get the eigenvalues of the energy for a variable position of the donor
!    !-----------------------------------------------------
!    RDif: IF (RDonornum>1) THEN
!        IF (NDonors==1) THEN
!            W = 0.0
!            RDdo: DO alpc = 1, RDonornum
!                RDonor = RDonorini+(alpc-1.0)*(RDonorend-RDonorini)/(RDonornum-1.0)
!                EigenE_RDonor(alpc,1) = RDonor
!                CALL HamiltonianMatrix(A,ADIM)
!                CALL ZHEEV("N", "U", 2*ADIM+1, A, 2*ADIM+1, W, WORK, LWORK, RWORK, INFO)
!                EigenE_RDonor(alpc, 2:2*ADIM+2) = W(1:2*ADIM+1)
!            END DO RDdo
!        ENDIF
!    ENDIF RDif
!
!    !The following lines get the eigenvalues of the energy for a variable delta
!    !-----------------------------------------------------
!    Deltaif: IF (deltanum>1) THEN
!        W = 0.0
!        Deltado: DO alpc = 1, deltanum
!
!            deltha = deltaini+(alpc-1.0)*(deltaend-deltaini)/(deltanum-1.0)
!            EigenE_delta(alpc,1) = deltha
!            CALL HamiltonianMatrix(A,ADIM)
!            CALL ZHEEV("N", "U", 2*ADIM+1, A, 2*ADIM+1, W, WORK, LWORK, RWORK, INFO)
!            EigenE_delta(alpc, 2:2*ADIM+2) = W(1:2*ADIM+1)
!
!        END DO deltado
!    END IF deltaif
!
!    !The following lines get the eigenvalues of the energy for a variable Vo Acceptor
!    !-----------------------------------------------------
!    VoAcceif: IF (VoAccenum>1) THEN
!        W = 0.0
!        VoAccedo: DO alpc = 1, VoAccenum
!
!            VoAcceptor = VoAcceini+(alpc-1.0)*(VoAcceend-VoAcceini)/(VoAccenum-1.0)
!            AcceptorTensor = VoAcceptor*AuxAcceptorTensor
!            EigenE_VoAcceptor(alpc,1) = VoAcceptor
!            CALL HamiltonianMatrix(A,ADIM)
!            CALL ZHEEV("N", "U", 2*ADIM+1, A, 2*ADIM+1, W, WORK, LWORK, RWORK, INFO)
!            EigenE_VoAcceptor(alpc, 2:2*ADIM+2) = W(1:2*ADIM+1)
!
!        END DO VoAccedo
!    END IF VoAcceif
!
!    !The following lines get the eigenvalues of the energy for a variable position of the acceptor
!    !-----------------------------------------------------
!!    RoAcif: IF (RoAccenum>1) THEN
!!        W = 0.0
!!        RoAcdo: DO alpc = 1, RoAccenum
!!            RoAcceptor = RoAcceini+(alpc-1.0)*(RoAcceend-RoAcceini)/(RoAccenum-1.0)
!!            IF (VoAcceptorPar .NE. 0) THEN
!!                AcceptorTensor = VoAcceptorPar*effectiveAcceptorPotencialTensor()
!!            ENDIF
!!            EigenE_RoAcceptor(alpc,1) = RoAcceptor
!!            CALL HamiltonianMatrix(A,ADIM)
!!            CALL ZHEEV("N", "U", 2*ADIM+1, A, 2*ADIM+1, W, WORK, LWORK, RWORK, INFO)
!!            EigenE_RoAcceptor(alpc, 2:2*ADIM+2) = W(1:2*ADIM+1)
!!
!!        END DO RoAcdo
!!    END IF RoAcif
!
!    !-----------------------------------------------------
!    !This code is to graph the effective acceptor potential
!!    DO i = 0, 499
!!        EffectiveAcceptorPotentialGraph(i+1,1) = -pi+2*pi*i/499
!!        !--
!!        EffectiveAcceptorPotentialGraph(i+1,2) = 20*REAL(ZAverage(AcceptorPotencial,DBLE(-pi+2*pi*i/499),1,1))
!!        EffectiveAcceptorPotentialGraph(i+1,3) = 70*REAL(ZAverage(AcceptorPotencial,DBLE(-pi+2*pi*i/499),1,1))
!!        EffectiveAcceptorPotentialGraph(i+1,4) = -100*REAL(ZAverage(AcceptorPotencial,DBLE(-pi+2*pi*i/499),1,1))
!!        EffectiveAcceptorPotentialGraph(i+1,2) = 0
!!        EffectiveAcceptorPotentialGraph(i+1,3) = REAL(CoulombPotencial(DBLE(-pi+2*pi*i/499),1,1))
!!        EffectiveAcceptorPotentialGraph(i+1,4) = 0
!        !--
!!        EffectiveAcceptorPotentialGraph(i+1,5) = -0*RcPar*COS(-pi+2*pi*i/499)
!!        EffectiveAcceptorPotentialGraph(i+1,6) = REAL(heightFunction(DBLE(-pi+2*pi*i/499), 1, 1))
!!        EffectiveAcceptorPotentialGraph(i+1,7) = EffectiveAcceptorPotentialGraph(i+1,4) &
!!                                                + EffectiveAcceptorPotentialGraph(i+1,5) &
!!                                                + EffectiveAcceptorPotentialGraph(i+1,6)
!!    ENDDO
!!--------------------------------------------------------
!
!
!
!    !-----------------------------------------------------
!    !The matrix that contains the data to be printed is filled
!!    Dat = 0.0
!!    Dat(1,1) = Nheavi
!!    Dat(2,1) = Rc
!!    Dat(3,:) = thetaPar*pi/180
!!    Dat(4,1:Nheavi) = Vo
!!    Dat(5,1) = 0.4
!!    Dat(6,1) = numRos
!    !-----------------------------------------------------
!    !The following lines are used to measure some observables of the system
!    !
!!    CALL MLz(A(:,31), Lz)
!!    WRITE(*,*) Lz
!
!    !-----------------------------------------------------
!    !The folowing lines write the resultant eigenvectors and eigenvalues in text files
!    CALL time_marker
!    CALL time_elapsed
!
!    WRITE(*,100)
!100 FORMAT(' ', 'Please insert the filename to create')
!    READ(*,*) filename
!
!    filenametemp = trim(filename)//"EigenVec.dat"
!    CALL MatrixWriter(EigenV, 2*ADIM+1, 2*ADIM+1, filenametemp)
!    filenametemp = trim(filename)//"EigenVal_AB.dat"
!    CALL MatrixWriter(EigenE_AB, gnum, 2*ADIM+2, filenametemp)
!    filenametemp = trim(filename)//"EigenVal_EF.dat"
!    CALL MatrixWriter(EigenE_EF, EFnum, 2*ADIM+2, filenametemp)
!    filenametemp = trim(filename)//"EigenVal_Alpha.dat"
!    CALL MatrixWriter(EigenE_Alpha, Alphanum, 2*ADIM+2, filenametemp)
!    filenametemp = trim(filename)//"EigenVal_Rc.dat"
!    CALL MatrixWriter(EigenE_Rc, Rcnum, 2*ADIM+2, filenametemp)
!    filenametemp = trim(filename)//"EigenVal_donor.dat"
!    CALL MatrixWriter(EigenE_RDonor, RDonornum, 2*ADIM+2, filenametemp)
!    filenametemp = trim(filename)//"EigenVal_delta.dat"
!    CALL MatrixWriter(EigenE_delta, deltanum, 2*ADIM+2, filenametemp)
!    filenametemp = trim(filename)//"EigenVal_VoAc.dat"
!    CALL MatrixWriter(EigenE_VoAcceptor, VoAccenum, 2*ADIM+2, filenametemp)
!    filenametemp = trim(filename)//"EigenVal_RoAc.dat"
!    CALL MatrixWriter(EigenE_RoAcceptor, RoAccenum, 2*ADIM+2, filenametemp)
!    filenametemp = trim(filename)//"EffAccPot.dat"
!    CALL MatrixWriter(EffectiveAcceptorPotentialGraph, 500, 7, filenametemp)
!    filenametemp = trim(filename)//"OptProp.dat"
!    CALL MatrixWriter(OpticalProp, hwnum, 7, filenametemp)
!    filenametemp = trim(filename)//"DensiProb.dat"
!    CALL MatrixWriter(DensiProbMatrix, numPoints, 2, filenametemp)
!    !Prints the data matrix that contains some values of the problem
!    filenametemp = trim(filename)//"Datos.dat"
!    CALL MatrixWriter(Dat, Ndat, 2*Nheavi, filenametemp)
!
!END PROGRAM
!
!SUBROUTINE initialConfiguration()
!    USE ParamModule
!    USE PhysModule
!    !Some constants are defined to give dimension to the variables
!    !Rydberg in meV
!    !Borh's radius in meters
!    epsil=4*pi*DIELEC(Pman,Temp)
!    Rydberg=e**4.0*EFECMASS(Xalma,Pman,Temp)*me/(2*hbar**2*(epsil*eo)**2)
!    Rydberg=Rydberg*6.2415093433E+21
!    a0=hbar**2*epsil*eo/(EFECMASS(Xalma,Pman,Temp)*me*e**2)
!END SUBROUTINE initialConfiguration
!
