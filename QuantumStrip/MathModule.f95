!   Purpose:
!       This module contains all of the mathematical functions and subroutines
!       used in the main program
!   Warning:
!       Anything must be changed here
MODULE MathModule
    USE ParamModule
    IMPLICIT NONE


CONTAINS
    !The following functions calculate the tensor elements of a heaviside function in the
    !complex exponential basis
    !----------------------------------------------------------
    FUNCTION heaviside(Nh, fh, fl, theta)

        COMPLEX*16, DIMENSION(2*ADIM+1,2*ADIM+1) :: heaviside, diagonal
        INTEGER :: Nh                !Number of barriers
        REAL, DIMENSION(2*Nh) :: theta !This array contains the value of each angular position
        REAL, DIMENSION(Nh) :: fh    !This array contains the value of each barriers
        REAL :: fl                  !This variable contains the lower limit of the heaviside function (The value of the well)
        INTEGER :: r,s,k            !Counters

        heaviside = CMPLX(0.0,0.0)
        diagonal = CMPLX(0.0, 0.0)

        DO r=-ADIM,ADIM
            DO s = -ADIM,ADIM

                !Adds the first and last term (thetai = 0 and thetaf=2pi)
                heaviside(r+ADIM+1,s+ADIM+1) = heaviside(r+ADIM+1,s+ADIM+1) + coefHeavi(fl, r, s, 0.0,theta(1))&
                + coefHeavi(fl, r, s, theta(2*Nh),2*pi)
                !Adds the second last term (This is done to simplify the next do loop)
                heaviside(r+ADIM+1,s+ADIM+1) = heaviside(r+ADIM+1,s+ADIM+1) + coefHeavi(fh(Nh), r, s, theta(2*Nh-1), theta(2*Nh))
                DO k =1, Nh-1

                    !Add the
                    heaviside(r+ADIM+1,s+ADIM+1) = heaviside(r+ADIM+1,s+ADIM+1) + coefHeavi(fh(k), r, s, theta(2*k-1), theta(2*k))&
                    +coefHeavi(fl, r, s, theta(2*k), theta(2*k+1))

                END DO

            END DO
        END DO
        heaviside = heaviside/(2*pi)

    END FUNCTION

    !Complex function used in the heaviside function
    FUNCTION coefHeavi(f, r, s, thetai, thetaf)

        COMPLEX*16 :: coefHeavi
        INTEGER :: r, s !Counters
        REAL :: f       !Value of the function
        REAL :: thetaf, thetafTemp  !Upper angular limit
        REAL :: thetai, thetaiTemp  !Lower angular limit
        REAL :: a,b     !Variables used in the function

        coefHeavi = 0.0
        thetafTemp = thetaf - INT(thetaf/2*pi)*2*pi
        thetaiTemp = thetai - INT(thetai/2*pi)*2*pi
        IF (r == s) THEN
            coefHeavi = f*(thetaf-thetai)
        ELSE
            a = +SIN((s-r)*thetafTemp)-SIN((s-r)*thetaiTemp)
            b = +COS((s-r)*thetaiTemp)-COS((s-r)*thetafTemp)
            coefHeavi = f*CMPLX(a,b)/(s-r)
        END IF

    END FUNCTION
    !----------------------------------------------------------
    SUBROUTINE intTrap(f,a,b,r,m,n)
        !    ===========================================================
        !     int_trap.f: integration by trapezoid rule of f(x) on [a,b]
        !     method:     trapezoid rule
        !     written by: Alex Godunov (March 2007)
        !    -------------------------------------------------
        !     f - Functions to integrate (supplied by a user)
        !     a - Lower limit of integration
        !     b - Upper limit of integration
        !     R - Result of integration (out)
        !     l - first subindex of the tensor
        !     m - second subindex of the tensor
        !    =================================================
        COMPLEX*16, EXTERNAL :: f
        DOUBLE PRECISION, INTENT(IN) :: a, b
        COMPLEX*16, INTENT(OUT) :: r
        DOUBLE PRECISION :: dx, x
        INTEGER, INTENT(IN) :: n, m
        INTEGER :: i

        r = CMPLX(0.0)
        dx = 0.0
        x = 0.0
        dx = (b-a)/numInt
        DO i=1,numInt-1
            x = a+i*dx
            r = r + f(x,m,n)
        END DO

        r = (r + (f(a,m,n)+f(b,m,n))/2.0)*dx

    END SUBROUTINE
<<<<<<< HEAD

    !----------------------------------------------------------
    !The following functions are used to calculate the values of the height tensor
    !Function to integrate
    FUNCTION heightMassFunction(phi, n, l)
        COMPLEX*16 :: heightMassFunction
        DOUBLE PRECISION :: inverseMass
        DOUBLE PRECISION :: phi                     !Argument of the function
        INTEGER :: l, n                  !First and second subindex, respectively, needed to calculate the heightTensor
        INTEGER :: i                     !Counter

        inverseMass = 1

        heightMassFunction = 0

        IF (Nheavi > 0) THEN
            IF (phi < thetaPar(1) .OR. phi > thetaPar(2*Nheavi)) inverseMass = 1

            IF (phi > thetaPar(2*Nheavi-1) .AND. phi < thetaPar(2*Nheavi)) inverseMass = ma*invmb(Nheavi)

            DO i = 1,Nheavi-1
                IF (phi > thetaPar(2*i-1) .AND. phi < thetaPar(2*i)) inverseMass = ma*invmb(i)

                IF (phi > thetaPar(2*i) .AND. phi < thetaPar(2*i+1)) inverseMass = 1
            ENDDO
        ENDIF

        IF (l == n) THEN
            heightMassFunction = 1.0/(hrib**2*(1.0-deltha*(1.0-COS(numRos*phi))))
        ELSE
            heightMassFunction = EXP(CMPLX(0, (l-n)*phi))/(hrib**2*(1.0-deltha*(1.0-COS(numRos*phi))))
        END IF

        heightMassFunction = heightMassFunction*inverseMass*pi**2

    END FUNCTION heightMassFunction

    !Efective Potencial function
    FUNCTION effectivePotencial(phi,n,l)

        COMPLEX*16 :: effectivePotencial
        DOUBLE PRECISION :: phi
        INTEGER :: n,l

        effectivePotencial = heightMassFunction(phi,n,l) + CoulombPotencial(phi,n,l)

        IF (l == n) THEN
            effectivePotencial = effectivePotencial - &
                                (heightFunctionDerivative(phi)/heightFunction(phi))**2 &
                                *(4*pi**2+3)/(12*Rc**2)
        ELSE
            effectivePotencial = effectivePotencial - &
                                (heightFunctionDerivative(phi)/heightFunction(phi))**2 &
                                *(4*pi**2+3)/(12*Rc**2) &
                                *EXP(CMPLX(0, (l-n)*phi))/(heightFunction(phi)**2)
        END IF


    END FUNCTION

    !Defines the height tensor integrating the height function
    FUNCTION effectivePotencialTensor()
        COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: effectivePotencialTensor, diagonal
        INTEGER :: r, s        !First and second subindex of the tensor

        effectivePotencialTensor = 0
        diagonal = 0
        DO r=-ADIM,ADIM
            DO s = r,ADIM

                CALL intTrap(effectivePotencial,DBLE(0.0), DBLE(2*pi), &
                            effectivePotencialTensor(r+ADIM+1, s+ADIM+1), r, s)

            END DO
            diagonal(r+ADIM+1,r+ADIM+1)=effectivePotencialTensor(r+ADIM+1,r+ADIM+1)
        END DO

        effectivePotencialTensor = effectivePotencialTensor + CONJG(TRANSPOSE(effectivePotencialTensor)) - diagonal
        effectivePotencialTensor = effectivePotencialTensor/(2*pi)

    END FUNCTION effectivePotencialTensor

    !Impurity acceptor function
    FUNCTION AcceptorPotencial(z,phi)

        REAL :: AcceptorPotencial
        DOUBLE PRECISION :: z, phi          !Values of the function

        AcceptorPotencial = EXP(-(Rc**2+RoAcceptor**2+(z-ZAcceptor)**2 &
                                             -2*Rc*RoAcceptor*COS(phi-PhiAcceptor)) &
                                             /dAcceptor**2)

        AcceptorPotencial = 2/heightFunction(phi) &
                            *(SIN(pi*z/heightFunction(phi)))**2 &
                            *AcceptorPotencial

    END FUNCTION AcceptorPotencial

    !effectiveAcceptorPotencialTensor
    !We do this function to minimize the computation time of the program
    FUNCTION effectiveAcceptorPotencialTensor()
        COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: effectiveAcceptorPotencialTensor, diagonal
        INTEGER :: r, s        !First and second subindex of the tensor

        effectiveAcceptorPotencialTensor = 0
        diagonal = 0
        DO r=-ADIM,ADIM
            DO s = r,ADIM

                CALL intTrap(effectiveAcceptorPotencial,DBLE(0.00), DBLE(2*pi), &
                            effectiveAcceptorPotencialTensor(r+ADIM+1, s+ADIM+1), r, s)

            END DO
            diagonal(r+ADIM+1,r+ADIM+1)=effectiveAcceptorPotencialTensor(r+ADIM+1,r+ADIM+1)
        END DO

        effectiveAcceptorPotencialTensor = effectiveAcceptorPotencialTensor + &
                                           CONJG(TRANSPOSE(effectiveAcceptorPotencialTensor)) &
                                           - diagonal
        effectiveAcceptorPotencialTensor = effectiveAcceptorPotencialTensor/(2*pi)

    END FUNCTION effectiveAcceptorPotencialTensor

    !Effective Acceptor function
    FUNCTION effectiveAcceptorPotencial(phi,n,l)

        COMPLEX*16 :: effectiveAcceptorPotencial
        DOUBLE PRECISION :: phi
        INTEGER :: n,l,i

        effectiveAcceptorPotencial = 0

        IF (l == n) THEN
            effectiveAcceptorPotencial = ZAverage(AcceptorPotencial,phi)*CMPLX(1,0)
        ELSE
            effectiveAcceptorPotencial = EXP(CMPLX(0, (l-n)*phi))*ZAverage(AcceptorPotencial,phi)
        END IF

    END FUNCTION

    !This is the height function for Donors and Acceptor Potencial
    FUNCTION heightFunction(phi)

        REAL :: heightFunction
        DOUBLE PRECISION :: phi

        heightFunction = hrib*(1.0-deltha*(1.0-COS(numRos*phi)))**0.5

    END FUNCTION heightFunction

    !This is the derivative of the height function for Donors and Acceptor Potencial
    FUNCTION heightFunctionDerivative(phi)

        REAL :: heightFunctionDerivative
        DOUBLE PRECISION :: phi

        heightFunctionDerivative = -deltha*numRos*hrib**2*SIN(numRos*phi) &
                                   /(2*heightFunction(phi))

    END FUNCTION heightFunctionDerivative

    !Average of the function
    FUNCTION ZAverage(f,phi)

        REAL :: ZAverage                     !Average of the function
        REAL, EXTERNAL :: f                 !Function to be averaged
        DOUBLE PRECISION :: phi             !Angle

        CALL integralPhi(f,phi, 0.0, heightFunction(phi), ZAverage)

    END FUNCTION ZAverage

    !The following function is used to calculate the Coulomb potencial of the electron
    !in the ribbon with one or more donors
    !Coulomb potencial
    FUNCTION CoulombPotencial(phi,n,l)

        COMPLEX*16 :: CoulombPotencial
        DOUBLE PRECISION :: phi                  !Variables of the function
        INTEGER :: i,l,n             !Counter

        CoulombPotencial = 0

        IF (NDonors > 0) THEN
            DO i=1, NDonors

                IF (NDonors .NE. 1) THEN

                    CoulombPotencial = CoulombPotencial - 2/SQRT(Rc**2+RoDonors(i)**2 &
                                       -2*Rc*RoDonors(i)*COS(phi-PhiDonors(i)) + (ZDonors(i)-0.2*heightFunction(phi))**2)
                ELSE

                    CoulombPotencial = CoulombPotencial - 2/SQRT(Rc**2+RDonor**2 &
                                       -2*Rc*RDonor*COS(phi-PhiDonors(i)) + (ZDonors(i)-0.2*heightFunction(phi))**2)

                ENDIF

            ENDDO
        ENDIF

        IF (l .NE. n) THEN
            CoulombPotencial = EXP(CMPLX(0, (l-n)*phi))*CoulombPotencial
        END IF

        IF (PARTICLE == "H") THEN
            CoulombPotencial = -CoulombPotencial
        ENDIF

    END FUNCTION CoulombPotencial

=======
!    !----------------------------------------------------------------
!    SUBROUTINE integral(f,a,b,r)
!!    ===========================================================
!!     int_trap.f: integration by trapezoid rule of f(x) on [a,b]
!!     method:     trapezoid rule
!!     written by: Alex Godunov (March 2007)
!!    -------------------------------------------------
!!     f - Function to integrate (supplied by a user)
!!     a - Lower limit of integration
!!     b - Upper limit of integration
!!     R - Result of integration (out)
!!    =================================================
!        REAL, INTENT(IN) :: a, b
!        REAL, EXTERNAL :: f
!        REAL, INTENT(OUT) :: r
!        DOUBLE PRECISION :: dx, x
!        INTEGER :: i
!
!        r = 0.0
!        dx = 0.0
!        x = 0.0
!        dx = (b-a)/numInt
!        DO i=1,numInt-1
!                x = a+i*dx
!                r = r + f(x)
!        END DO
!        r = (r + (f(a) + f(b))/2.0)*dx
!    END SUBROUTINE
!!
!    SUBROUTINE integralPhi(f,phi,a,b,r)
!!    ===========================================================
!!     int_trap.f: integration by trapezoid rule of f(x) on [a,b]
!!     method:     trapezoid rule
!!     written by: Alex Godunov (March 2007)
!!    -------------------------------------------------
!!     f - Function to integrate (supplied by a user)
!!     a - Lower limit of integration
!!     b - Upper limit of integration
!!     R - Result of integration (out)
!!    =================================================
!        REAL, INTENT(IN) :: a, b
!        DOUBLE PRECISION, INTENT(IN) :: phi
!        REAL, EXTERNAL :: f
!        REAL, INTENT(OUT) :: r
!        DOUBLE PRECISION :: dx, x
!        INTEGER :: i,j
!
!        r = 0.0
!        dx = 0.0
!        x = 0.0
!        dx = (b-a)/numInt
!        DO i=1,numInt-1
!                x = a+i*dx
!                r = r + f(x,phi)
!        END DO
!
!        r = (r + (f(a,phi) + f(b,phi))/2.0)*dx
!
!    END SUBROUTINE
!
!    !----------------------------------------------------------
!    !The following functions are used to calculate the values of the height tensor
!    !Function to integrate
!    FUNCTION heightMassFunction(phi, n, l)
!        COMPLEX*16 :: heightMassFunction
!        DOUBLE PRECISION :: inverseMass
!        DOUBLE PRECISION :: phi                     !Argument of the function
!        INTEGER :: l, n                  !First and second subindex, respectively, needed to calculate the heightTensor
!        INTEGER :: i                     !Counter
!
!        inverseMass = 1
!
!        heightMassFunction = 0
!
!        IF (Nheavi > 0) THEN
!            IF (phi < thetaPar(1) .OR. phi > thetaPar(2*Nheavi)) inverseMass = 1
!
!            IF (phi > thetaPar(2*Nheavi-1) .AND. phi < thetaPar(2*Nheavi)) inverseMass = ma*invmb(Nheavi)
!
!            DO i = 1,Nheavi-1
!                IF (phi > thetaPar(2*i-1) .AND. phi < thetaPar(2*i)) inverseMass = ma*invmb(i)
!
!                IF (phi > thetaPar(2*i) .AND. phi < thetaPar(2*i+1)) inverseMass = 1
!            ENDDO
!        ENDIF
!
!        IF (l == n) THEN
!            heightMassFunction = 1.0/(hrib**2*(1.0-deltha*(1.0-COS(numRos*phi))))
!        ELSE
!            heightMassFunction = EXP(CMPLX(0, (l-n)*phi))/(hrib**2*(1.0-deltha*(1.0-COS(numRos*phi))))
!        END IF
!
!        heightMassFunction = heightMassFunction*inverseMass*pi**2
!
!    END FUNCTION heightMassFunction
!
!    !Efective Potencial function
!    FUNCTION effectivePotencial(phi,n,l)
!
!        COMPLEX*16 :: effectivePotencial
!        DOUBLE PRECISION :: phi
!        INTEGER :: n,l
!
!        effectivePotencial = heightMassFunction(phi,n,l) + CoulombPotencial(phi,n,l)
!
!        IF (l == n) THEN
!            effectivePotencial = effectivePotencial - &
!                                (heightFunctionDerivative(phi)/heightFunction(phi))**2 &
!                                *(4*pi**2+3)/12
!        ELSE
!            effectivePotencial = effectivePotencial - &
!                                (heightFunctionDerivative(phi)/heightFunction(phi))**2 &
!                                *(4*pi**2+3)/12 &
!                                *EXP(CMPLX(0, (l-n)*phi))/(heightFunction(phi)**2)
!        END IF
!
!
!    END FUNCTION
!
!    !Defines the height tensor integrating the height function
!    FUNCTION effectivePotencialTensor()
!        COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: effectivePotencialTensor, diagonal
!        INTEGER :: r, s        !First and second subindex of the tensor
!
!        effectivePotencialTensor = 0
!        diagonal = 0
!        DO r=-ADIM,ADIM
!            DO s = r,ADIM
!
!                CALL intTrap(effectivePotencial,DBLE(0.0), DBLE(2*pi), &
!                            effectivePotencialTensor(r+ADIM+1, s+ADIM+1), r, s)
!
!            END DO
!            diagonal(r+ADIM+1,r+ADIM+1)=effectivePotencialTensor(r+ADIM+1,r+ADIM+1)
!        END DO
!
!        effectivePotencialTensor = effectivePotencialTensor + CONJG(TRANSPOSE(effectivePotencialTensor)) - diagonal
!        effectivePotencialTensor = effectivePotencialTensor/(2*pi)
!
!    END FUNCTION effectivePotencialTensor
!
!    !Impurity acceptor function
!    FUNCTION AcceptorPotencial(z,phi)
!
!        REAL :: AcceptorPotencial
!        DOUBLE PRECISION :: z, phi          !Values of the function
!
!        AcceptorPotencial = EXP(-(Rc**2+RoAcceptor**2+(z-ZAcceptor)**2 &
!                                             -2*Rc*RoAcceptor*COS(phi-PhiAcceptor)) &
!                                             /dAcceptor**2)
!
!        AcceptorPotencial = 2/heightFunction(phi) &
!                            *(SIN(pi*z/heightFunction(phi)))**2 &
!                            *AcceptorPotencial
!
!    END FUNCTION AcceptorPotencial
!
!    !effectiveAcceptorPotencialTensor
!    !We do this function to minimize the computation time of the program
!    FUNCTION effectiveAcceptorPotencialTensor()
!        COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: effectiveAcceptorPotencialTensor, diagonal
!        INTEGER :: r, s        !First and second subindex of the tensor
!
!        effectiveAcceptorPotencialTensor = 0
!        diagonal = 0
!        DO r=-ADIM,ADIM
!            DO s = r,ADIM
!
!                CALL intTrap(effectiveAcceptorPotencial,DBLE(0.00), DBLE(2*pi), &
!                            effectiveAcceptorPotencialTensor(r+ADIM+1, s+ADIM+1), r, s)
!
!            END DO
!            diagonal(r+ADIM+1,r+ADIM+1)=effectiveAcceptorPotencialTensor(r+ADIM+1,r+ADIM+1)
!        END DO
!
!        effectiveAcceptorPotencialTensor = effectiveAcceptorPotencialTensor + &
!                                           CONJG(TRANSPOSE(effectiveAcceptorPotencialTensor)) &
!                                           - diagonal
!        effectiveAcceptorPotencialTensor = effectiveAcceptorPotencialTensor/(2*pi)
!
!    END FUNCTION effectiveAcceptorPotencialTensor
!
!    !Effective Acceptor function
!    FUNCTION effectiveAcceptorPotencial(phi,n,l)
!
!        COMPLEX*16 :: effectiveAcceptorPotencial
!        DOUBLE PRECISION :: phi
!        INTEGER :: n,l,i
!
!        effectiveAcceptorPotencial = 0
!
!        IF (l == n) THEN
!            effectiveAcceptorPotencial = ZAverage(AcceptorPotencial,phi)*CMPLX(1,0)
!        ELSE
!            effectiveAcceptorPotencial = EXP(CMPLX(0, (l-n)*phi))*ZAverage(AcceptorPotencial,phi)
!        END IF
!
!    END FUNCTION
!
!    !This is the height function for Donors and Acceptor Potencial
!    FUNCTION heightFunction(phi)
!
!        REAL :: heightFunction
!        DOUBLE PRECISION :: phi
!
!        heightFunction = hrib*(1.0-deltha*(1.0-COS(numRos*phi)))**0.5
!
!    END FUNCTION heightFunction
!
!    !This is the derivative of the height function for Donors and Acceptor Potencial
!    FUNCTION heightFunctionDerivative(phi)
!
!        REAL :: heightFunctionDerivative
!        DOUBLE PRECISION :: phi
!
!        heightFunctionDerivative = -deltha*numRos*hrib**2*SIN(numRos*phi) &
!                                   /(2*heightFunction(phi))
!
!    END FUNCTION heightFunctionDerivative
!
!    !Average of the function
!    FUNCTION ZAverage(f,phi)
!
!        REAL :: ZAverage                     !Average of the function
!        REAL, EXTERNAL :: f                 !Function to be averaged
!        DOUBLE PRECISION :: phi             !Angle
!
!        CALL integralPhi(f,phi, 0.0, heightFunction(phi), ZAverage)
!
!    END FUNCTION ZAverage
!
!    !The following function is used to calculate the Coulomb potencial of the electron
!    !in the ribbon with one or more donors
!    !Coulomb potencial
!    FUNCTION CoulombPotencial(phi,n,l)
!
!        COMPLEX*16 :: CoulombPotencial
!        DOUBLE PRECISION :: phi                  !Variables of the function
!        INTEGER :: i,l,n             !Counter
!
!        CoulombPotencial = 0
!
!        IF (NDonors > 0) THEN
!            DO i=1, NDonors
!
!                IF (NDonors .NE. 1) THEN
!
!                    CoulombPotencial = CoulombPotencial - 2/SQRT(Rc**2+RoDonors(i)**2 &
!                                       -2*Rc*RoDonors(i)*COS(phi-PhiDonors(i)) + (ZDonors(i)-0.2*heightFunction(phi))**2)
!                ELSE
!
!                    CoulombPotencial = CoulombPotencial - 2/SQRT(Rc**2+RDonor**2 &
!                                       -2*Rc*RDonor*COS(phi-PhiDonors(i)) + (ZDonors(i)-0.2*heightFunction(phi))**2)
!
!                ENDIF
!
!            ENDDO
!        ENDIF
!
!        IF (l .NE. n) THEN
!            CoulombPotencial = EXP(CMPLX(0, (l-n)*phi))*CoulombPotencial
!        END IF
!
!    END FUNCTION CoulombPotencial
>>>>>>> severalChangesBranch

END MODULE MathModule

