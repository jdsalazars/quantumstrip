!The subroutines in this module are defined to measure an observable quantity

MODULE Measurements
USE ParamModule
IMPLICIT NONE


    CONTAINS
    !This subroutine measures the z-component of the angular momentum
    !of the wave function
    SUBROUTINE MLz(wave ,Lz)

        COMPLEX*16, DIMENSION(2*ADIM+1), INTENT(IN) :: wave !Input argument
        COMPLEX*16 :: Lzc   !Variable used to calculate lz
        DOUBLE PRECISION, INTENT(OUT) :: Lz !Value of Lz for the wave function
        !Counter
        INTEGER :: i

        Lz = 0.0
        Lzc = (0.0,0.0)

        !Lz is equal to the sum of the squared module of all expansion coeficients
        !Multiplied by its energy level "n"
        DO i=-ADIM, ADIM

            Lzc = Lzc + wave(i+ADIM+1)*CONJG(wave(i+ADIM+1))*i

        END DO
        Lz = DBLE(Lzc)

    END SUBROUTINE

END MODULE Measurements
