!   Purpose:
!       This module contains all the functions, subroutines and parameters needed
!       to calculate the optical properties of the system
!   Warning:
!       Don't change anything here

MODULE OpticalProperties
USE MathModule
USE ParamModule
USE PhysModule

IMPLICIT NONE

    CONTAINS

    !This function computes the difference in energy between the first two states of the
    !system
    FUNCTION Eij(EigenValues,i,j)

        DOUBLE PRECISION, DIMENSION(2*ADIM+1) :: EigenValues    !Eigenvalues of the hamiltonian matrix
        DOUBLE PRECISION :: Eij                                 !Energy difference
        INTEGER :: i,j

        Eij = EigenValues(i) - EigenValues(j)

!        WRITE(*,*)E21

    END FUNCTION Eij

    !This function computes the dipole transition from state i to state j
    FUNCTION Mij(EigenVectors,i,j)

        COMPLEX*16 :: Mij                                           !Electric dipole
        COMPLEX*16 :: Mij1                                           !Electric dipole
        COMPLEX*16 :: Mij2                                           !Electric dipole
        COMPLEX*16 :: Mij3                                           !Electric dipole
        COMPLEX*16, DIMENSION(2*ADIM+1,2*ADIM+1) :: EigenVectors    !Eigenvectors of the hamiltonian matrix
        INTEGER :: i,j  !States to compute the transition
        INTEGER :: m    !Counter

        Mij = 0
        Mij1 = 0
        Mij2 = 0
        Mij3 = 0

        DO m=1,2*ADIM
            Mij1 = Mij1 + CONJG(EigenVectors(m,i))*EigenVectors(m+1,j)
        ENDDO

        DO m=2,2*ADIM+1
            Mij2 = Mij2 + CONJG(EigenVectors(m,i))*EigenVectors(m-1,j)
        ENDDO

        Mij = -CMPLX(0,1.0)*e*ribRadiuspar*a0*(Mij1-Mij2)/2.0

        IF (NDonors == 1) THEN

            DO m=1,2*ADIM+1
                Mij3 = Mij3 + CONJG(EigenVectors(m,i))*EigenVectors(m,j)
            ENDDO

           Mij =  Mij - Mij3*e*RoDonors(1)*SIN(PhiDonors(1))*a0/2.0

        ENDIF
!        WRITE(*,*)Mij*CONJG(Mij),i,j

    END FUNCTION Mij

    !This function computes the linear susceptibility
    FUNCTION LSuscept(hw,A,W,i,j)

        COMPLEX*16 :: LSuscept      !Linear susceptibility
        DOUBLE PRECISION :: hw      !Energy of the system
        COMPLEX*16, DIMENSION(2*ADIM+1,2*ADIM+1) :: A
        DOUBLE PRECISION, DIMENSION(2*ADIM+1) :: W
        INTEGER :: i,j


        LSuscept = (Nden/eo)*CONJG(Mij(A,i,j))*Mij(A,i,j)/(Eij(W,i,j)-hw-CMPLX(0,1)*hbar*gamma12)

    END FUNCTION LSuscept

    !This function computes the non-linear susceptibility
    FUNCTION NLSuscept(hw,A,W,i,j)

        COMPLEX*16 :: NLSuscept     !Non-Linear susceptibility
        DOUBLE PRECISION :: hw      !Energy of the system
        COMPLEX*16, DIMENSION(2*ADIM+1,2*ADIM+1) :: A
        DOUBLE PRECISION, DIMENSION(2*ADIM+1) :: W
        INTEGER :: i,j

        NLSuscept = -(Nden/eo)* &
                    (Intensity/(2*nr)) &
                    *mu0*c*CONJG(Mij(A,i,j))*Mij(A,i,j)/(Eij(W,i,j)-hw-CMPLX(0,1)*hbar*gamma12) &
                    *(4*CONJG(Mij(A,i,j))*Mij(A,i,j)/((Eij(W,i,j)-hw)**2+(hbar*gamma12)**2) &
                    -(REAL(Mij(A,i,i)-Mij(A,j,j)))**2/((Eij(W,i,j)-CMPLX(0,1)*hbar*gamma12)* &
                    (Eij(W,i,j)-hw-CMPLX(0,1)*hbar*gamma12)))

    END FUNCTION NLSuscept

    !This subroutine calculates the linear and third-order non-linear refractive index
    !changes and absortion coeffiencients
    SUBROUTINE OptProp(hw,A,W,LIndex,NLIndex,LAbs,NLAbs,i,j)

        IMPLICIT NONE

        DOUBLE PRECISION, INTENT(IN) :: hw          !Energy variable
        COMPLEX*16, INTENT(IN), DIMENSION(2*ADIM+1,2*ADIM+1):: A
        DOUBLE PRECISION, INTENT(IN), DIMENSION(2*ADIM+1) :: W
        DOUBLE PRECISION, INTENT(OUT) :: LIndex          !Linear refractive index change
        DOUBLE PRECISION, INTENT(OUT) :: NLIndex         !Non-Linear refractive index change
        DOUBLE PRECISION, INTENT(OUT) :: LAbs            !Linear absorbtion coefficient
        DOUBLE PRECISION, INTENT(OUT) :: NLAbs           !Non-Linear absorbtion coefficient
        INTEGER, INTENT(IN) :: i,j                       !States for the optical properties

        LIndex = REAL(LSuscept(hw,A,W,i,j))/(2*nr**2)
        NLIndex = REAL(NLSuscept(hw,A,W,i,j))/(2*nr**2)
        LAbs = AIMAG(LSuscept(hw,A,W,i,j))*eo*(hw/hbar)*SQRT(mu0/(DIELEC(Pman,Temp)*eo))
        NLAbs = AIMAG(NLSuscept(hw,A,W,i,j))*eo*(hw/hbar)*SQRT(mu0/(DIELEC(Pman,Temp)*eo))

    END SUBROUTINE OptProp

!----------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------
!This section is to check the values of the optical properties
!
!    FUNCTION LinearIndex(hw,A,W)
!
!        DOUBLE PRECISION :: LinearIndex
!        DOUBLE PRECISION, INTENT(IN) :: hw          !Energy variable
!        COMPLEX*16, INTENT(IN), DIMENSION(2*ADIM+1,2*ADIM+1):: A
!        DOUBLE PRECISION, INTENT(IN), DIMENSION(2*ADIM+1) :: W
!
!        LinearIndex = 1/(2*eo*nr**2)*REAL(Mij(A,2,1)*CONJG(Mij(A,2,1)))*Nden* &
!                      (Eij(W,2,1)-hw)/((Eij(W,2,1)-hw)**2+(hbar*gamma12)**2)
!
!    END FUNCTION LinearIndex
!
!    FUNCTION NonLinearIndex(hw,A,W)
!
!        DOUBLE PRECISION :: NonLinearIndex
!        DOUBLE PRECISION, INTENT(IN) :: hw          !Energy variable
!        COMPLEX*16, INTENT(IN), DIMENSION(2*ADIM+1,2*ADIM+1):: A
!        DOUBLE PRECISION, INTENT(IN), DIMENSION(2*ADIM+1) :: W
!
!        NonLinearIndex = -mu0*c/(4*nr**3*eo)*REAL(Mij(A,2,1)*CONJG(Mij(A,2,1)))*Nden*Intensity &
!                        /((Eij(W,2,1)-hw)**2+(hbar*gamma12)**2)**2 &
!                        *((Eij(W,2,1)-hw)*4*REAL(Mij(A,2,1)*CONJG(Mij(A,2,1))))
!
!    END FUNCTION NonLinearIndex
!
!    FUNCTION LinearAbs(hw,A,W)
!
!        DOUBLE PRECISION :: LinearAbs
!        DOUBLE PRECISION, INTENT(IN) :: hw          !Energy variable
!        COMPLEX*16, INTENT(IN), DIMENSION(2*ADIM+1,2*ADIM+1):: A
!        DOUBLE PRECISION, INTENT(IN), DIMENSION(2*ADIM+1) :: W
!
!        LinearAbs = (hw/hbar)*SQRT(mu0/(DIELEC(Pman,Temp)*eo))*REAL(Mij(A,2,1)*CONJG(Mij(A,2,1)) &
!                    *Nden*hbar*gamma12/((Eij(W,2,1)-hw)**2+(hbar*gamma12)**2))
!
!    END FUNCTION LinearAbs
!
!FUNCTION NonLinearAbs(hw,A,W)
!
!        DOUBLE PRECISION :: NonLinearAbs
!        DOUBLE PRECISION, INTENT(IN) :: hw          !Energy variable
!        COMPLEX*16, INTENT(IN), DIMENSION(2*ADIM+1,2*ADIM+1):: A
!        DOUBLE PRECISION, INTENT(IN), DIMENSION(2*ADIM+1) :: W
!
!        NonLinearAbs = -(hw/hbar)*SQRT(mu0/(DIELEC(Pman,Temp)*eo))*Intensity/(2*eo*nr*c) &
!                       *REAL(Mij(A,2,1)*CONJG(Mij(A,2,1)))*Nden*hbar*gamma12 &
!                       /(((Eij(W,2,1)-hw)**2+(hbar*gamma12)**2)**2)*4*REAL(Mij(A,2,1)*CONJG(Mij(A,2,1)))
!
!    END FUNCTION NonLinearAbs

END MODULE OpticalProperties
