!   Purpose:
!       This module contains all of the parameters
!   Warnings:
!       All of the parameters introduced must be adimensional
!
MODULE ParamModule
IMPLICIT NONE

    !Mathematical constants
    REAL, PARAMETER :: pi = 3.1415926535897932
    !Physical constants
    DOUBLE PRECISION, PARAMETER :: e = 1.602176565E-19      !Electron charge
    DOUBLE PRECISION, PARAMETER :: me = 9.10938291E-31      !Mass of the electron
    DOUBLE PRECISION, PARAMETER :: hbar = 1.0545718E-34     !Planck constant
    DOUBLE PRECISION, PARAMETER :: eo = 8.8541878176E-12    !Vaccuum permitivity
    DOUBLE PRECISION, PARAMETER :: mu0 = 4*pi*1E-7          !Vaccuum permeability
    DOUBLE PRECISION, PARAMETER :: c = 299792458            !Speed of light in vaccuum
    DOUBLE PRECISION, PARAMETER :: nr=3.2                   !Refractive index of AlGaAs
    DOUBLE PRECISION, PARAMETER :: gamma12=5.E12            !Inverse of the relaxation time
    DOUBLE PRECISION, PARAMETER :: Nden=1.0E22              !Carrier density of the AlGaAs
    DOUBLE PRECISION :: Rydberg                         !Adimensional unit of energy
    DOUBLE PRECISION :: a0                              !Adimensional unit of distance
    DOUBLE PRECISION :: epsil                           !Coulombian constant
    !----------------------------------------------------------------
    !Matrix parameters
    INTEGER, PARAMETER :: ADIM = 51         !2*ADIM+1 is the dimension of the matrix
    !----------------------------------------------------------------
    !Electron-Hole Parameters
    CHARACTER, PARAMETER :: PARTICLE = "E"  !Write "E" to work with an electron or type
                                            !"H" to work with a light-hole
    !----------------------------------------------------------------
    !General parameters
    REAL, PARAMETER :: ribRadiuspar = 2.0             !Radious of the ribbon (In Bohr radious)
    REAL, PARAMETER :: magneticFieldpar = 0.00                         !Adimensional magnetic field magnitude
    REAL, PARAMETER :: elecFieldPar = 0.00               !Adimensional electric field magnitude
    REAL, PARAMETER :: thetaEFieldPar = 0.00        !Angle of the electric field respect to the x-axis
    REAL, PARAMETER :: hrib = 0.2               !Adimensional ribbon height
    REAL, PARAMETER :: Pman = 0.0           !Manometric presure
    REAL, PARAMETER :: Temp = 4.0          !Temperature in °K
<<<<<<< HEAD

    !Parameters of the donors in the ribbon
    INTEGER, PARAMETER :: Ndonors = 1        !Number of donors in the ribbon
    REAL, DIMENSION(Ndonors) :: RoDonors = (/0.0/)    !Distance from the origin in the xy plane at which the donors are placed.
    REAL, DIMENSION(Ndonors) :: PhiDonors = (/0.0/)   !Angle at which the donors are placed.  This angles go form 0� to 360�
    REAL, DIMENSION(Ndonors) :: ZDonors  = (/0.0/)    !Height at which the donors are placed.
    REAL, PARAMETER :: RDonorini = 0.9          !First value of the acceptor position
    REAL, PARAMETER :: RDonorend = 2.5           !Last value of the acceptor position
    INTEGER, PARAMETER :: RDonornum = 1          !Number of values of the acceptor position, It needs to be at least 2.
                                            !If you don't want to get Aaranov-Bohn curves put acceptor potentialnum = 1
    REAL :: RDonor                               !Variable value of the magnetic field

    !Paremeters of the accpetor in the ribbon
    REAL :: RoAcceptorPar = 0.0    !Distance from the origin in the xy plane at which the acceptor placed.
    REAL :: PhiAcceptor = 0.0    !Angle at which the acceptor is placed.  This angle go form 0 to 2pi
    REAL :: ZAcceptor  = 0.0    !Height at which the acceptor is placed.
    REAL :: VoAcceptorPar = 0.0                  !Potential of the accpetor in effective Rydbergs
    REAL :: dAcceptor = 1.0                    !Width of the accpetor in effective bohr radius
    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: AcceptorTensor  !Tensor for the acceptor impurity
    COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: AuxAcceptorTensor
!    INTEGER, PARAMETER :: numAccePot = 2000
!    DOUBLE PRECISION, DIMENSION(numAccePot+1,2):: AcceptorPotentialArray
    !---
    REAL, PARAMETER :: RoAcceini = 0.0          !First value of the acceptor position
    REAL, PARAMETER :: RoAcceend = 2.0           !Last value of the acceptor position
    INTEGER, PARAMETER :: RoAccenum = 1           !Number of values of the acceptor position, It needs to be at least 2.
                                            !If you don't want to get Aaranov-Bohn curves put acceptor potentialnum = 1
    REAL :: RoAcceptor                               !Variable value of the magnetic field
    !--
    REAL, PARAMETER :: VoAcceini = 0.0          !First value of the acceptor potential
    REAL, PARAMETER :: VoAcceend = 100.0           !Last value of the acceptor potential
    INTEGER, PARAMETER :: VoAccenum = 1           !Number of values of the acceptor potential, It needs to be at least 2.
                                            !If you don't want to get Aaranov-Bohn curves put acceptor potentialnum = 1
    REAL :: VoAcceptor                               !Variable value of the magnetic field


=======
    !----------------------------------------------------------------
>>>>>>> severalChangesBranch
    !Parameters to get Aaranov-Bohn oscillations
    REAL, PARAMETER :: gini = 0.0          !First value of the adimensional magnetic field
    REAL, PARAMETER :: gend = 4.0           !Last value of the adimensional magnetic field
    INTEGER, PARAMETER :: gnum = 100           !Number of values of g, It needs to be at least 1.
    !----------------------------------------------------------------
    !Parameters of variable electric field
<<<<<<< HEAD
    REAL :: elecField                   !Variable value of the adimensional electric field
    REAL  :: thetaEField = 0.0        !Angle of the electric field respect to the x-axis
    REAL, PARAMETER :: EFini = -40.0          !First value of the adimensional electric field
    REAL, PARAMETER :: EFend = 40.0           !Last value of the adimensional electric field
    INTEGER, PARAMETER :: EFnum = 100         !Number of values of etha, It needs to be at least 2.
                                            !If you don't want to get Energy-ElectricField curves put EFnum = 1

    !Parameters of variable angle alfa between two barriers
    REAL :: alpha                   !Variable value of the angle alpha
    REAL, DIMENSION(2), PARAMETER :: betha = (/10.0, 10.0/)  !Angle of each barrier
    REAL :: Alphaini =  0.0                !First value of the angle alpha
    REAL :: Alphaend = 360.0 - betha(1) - betha(2)!Last value of the angle alpha
    INTEGER, PARAMETER :: Alphanum = 1         !Number of values of alpha, It needs to be at least 2.
    REAL, DIMENSION(2*Nheavi) :: theta !Angular widht (In degrees) of the potential barrier

=======
    REAL, PARAMETER :: EFini = -10.0          !First value of the adimensional electric field
    REAL, PARAMETER :: EFend = 10.0           !Last value of the adimensional electric field
    INTEGER, PARAMETER :: EFnum = 0         !Number of values of etha, It needs to be at least 2.
                                            !If you don't want to get Energy-ElectricField curves put EFnum = 0
    !----------------------------------------------------------------
>>>>>>> severalChangesBranch
    !Parameters of variable radius
    REAL, PARAMETER :: Rcini = 1.0          !First value of the adimensional electric field
    REAL, PARAMETER :: Rcend = 3.0           !Last value of the adimensional electric field
    INTEGER, PARAMETER :: Rcnum = 1         !Number of values of Rc, It needs to be at least 2.
                                            !If you don't want to get Energy-Radius curves put Rcnum = 1
<<<<<<< HEAD

    !Parameters of variable height
    INTEGER, PARAMETER :: numRos = 2           !Number of peaks in the ribbon
    INTEGER,PARAMETER :: numInt = 500          !Number of intervals to integrate
    REAL, PARAMETER :: hrib = 0.2               !Adimensional ribbon height
    REAL, PARAMETER :: deltapar = 0.1              !Parameter to choose the height of the peaks in the ribbon

    !Parameters of variable delta
    REAL :: deltha                   !Variable value of the adimensional radius
    REAL, PARAMETER :: deltaini = 0.0          !First value of the adimensional electric field
    REAL, PARAMETER :: deltaend = 0.2           !Last value of the adimensional electric field
    INTEGER, PARAMETER :: deltanum = 1         !Number of values of delta, It needs to be at least 2.
                                            !If you don't want to get Energy-delta curves put Deltanum = 1

    !Optical parameters
    DOUBLE PRECISION, PARAMETER :: Intensity = 0.05E10        !Optical intensity in W/m^2
    DOUBLE PRECISION :: hw                                 !Variable value of the energy for the OptProp
    DOUBLE PRECISION, PARAMETER :: hwini = 0.0          !First value of the energy
    DOUBLE PRECISION, PARAMETER :: hwend = 20.0           !Last value of the energy
    INTEGER, PARAMETER :: hwnum = 2000        !Number of values of the energy, It needs to be at least 2.
=======
    !----------------------------------------------------------------
    !Parameters of the roughness of the ribbon
    INTEGER, PARAMETER :: numRos = 0           !Number of peaks in the ribbon
    REAL, PARAMETER :: deltapar = 0.1              !Parameter to choose the height of the peaks in the ribbon
    INTEGER,PARAMETER :: numInt = 2000          !Number of intervals to integrate
    !----------------------------------------------------------------
    !Azimutal barrier parameters
    INTEGER, PARAMETER :: Nheavi = 1        !Number of barriers in the ribbon
    REAL, DIMENSION(2*Nheavi) :: thetaBarrierPar = (/37.000, 57.000/)!Angular widht (In degrees) of the potential barrier
    REAL, PARAMETER :: XalmaPar = 0.000                     !Concentration of Al in a GaAlAs material with mass mb
    REAL, DIMENSION(Nheavi), PARAMETER :: XalmbPar = (/0.200/)     !Concentration of Al in a GaAlAs material with mass mb
    !----------------------------------------------------------------
    !Parameters of the donors in the ribbon
    INTEGER, PARAMETER :: Ndonors = 1        !Number of donors in the ribbon
    REAL, DIMENSION(Ndonors) :: RoDonors = (/0.0/)    !Distance from the origin in the xy plane at which the donors are placed.
    REAL, DIMENSION(Ndonors) :: PhiDonors = (/0.0/)   !Angle at which the donors are placed.  This angles go form 0� to 360�
    REAL, DIMENSION(Ndonors) :: ZDonors  = (/0.0/)    !Height at which the donors are placed.
    REAL, PARAMETER :: RDonorini = 0.0          !First value of the acceptor position
    REAL, PARAMETER :: RDonorend = 2.0           !Last value of the acceptor position
    INTEGER, PARAMETER :: RDonornum = 1           !Number of values of the acceptor position, It needs to be at least 2.
    !---------------------------------------------------------------
    !Paremeters of the accpetor in the ribbon
    !REAL :: RoAcceptorPar = 0.0    !Distance from the origin in the xy plane at which the acceptor placed.
    !REAL :: PhiAcceptor = 0.0    !Angle at which the acceptor is placed.  This angle go form 0 to 2pi
    !REAL :: ZAcceptor  = 0.0    !Height at which the acceptor is placed.
    !REAL :: VoAcceptorPar = 0.0                  !Potential of the accpetor in effective Rydbergs
    !REAL :: dAcceptor = 1.0                    !Width of the accpetor in effective bohr radius
    !COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: AcceptorTensor  !Tensor for the acceptor impurity
    !COMPLEX*16, DIMENSION(2*ADIM+1, 2*ADIM+1) :: AuxAcceptorTensor
    !   INTEGER, PARAMETER :: numAccePot = 2000
    !   DOUBLE PRECISION, DIMENSION(numAccePot+1,2):: AcceptorPotentialArray
    !----------------------------------------------------------------
    !Parameters of variable position of the acceptor
    !REAL, PARAMETER :: RoAcceini = 0.0          !First value of the acceptor position
    !REAL, PARAMETER :: RoAcceend = 2.0           !Last value of the acceptor position
    !INTEGER, PARAMETER :: RoAccenum = 1           !Number of values of the acceptor position, It needs to be at least 2.
    !----------------------------------------------------------------
    !Parameters of the variable potential of the acceptor
    !REAL, PARAMETER :: VoAcceini = 0.0          !First value of the acceptor potential
    !REAL, PARAMETER :: VoAcceend = 100.0           !Last value of the acceptor potential
    !INTEGER, PARAMETER :: VoAccenum = 1           !Number of values of the acceptor potential, It needs to be at least 2.
    !----------------------------------------------------------------
    !Parameters of variable angle alfa between two barriers
    !REAL, DIMENSION(2), PARAMETER :: betha = (/10.0, 10.0/)  !Angle of each barrier
    !REAL :: Alphaini =  0.0                !First value of the angle alpha
    !REAL :: Alphaend = 360.0 - betha(1) - betha(2)!Last value of the angle alpha
    !INTEGER, PARAMETER :: Alphanum = 1         !Number of values of alpha, It needs to be at least 2.
    !REAL, DIMENSION(2*Nheavi) :: theta !Angular widht (In degrees) of the potential barrier
    !----------------------------------------------------------------
    !Parameters of the variable amplitude of the roughness
    !REAL, PARAMETER :: deltaini = 0.0          !First value of the adimensional electric field
    !REAL, PARAMETER :: deltaend = 0.3           !Last value of the adimensional electric field
    !INTEGER, PARAMETER :: deltanum = 1         !Number of values of delta, It needs to be at least 2.
    !----------------------------------------------------------------
    !Optical parameters
    DOUBLE PRECISION, PARAMETER :: Intensity = 0.15E10        !Optical intensity in W/m^2
    DOUBLE PRECISION, PARAMETER :: hwini = 0.0          !First value of the energy
    DOUBLE PRECISION, PARAMETER :: hwend = 50.0           !Last value of the energy
    INTEGER, PARAMETER :: hwnum = 1         !Number of values of the energy, It needs to be at least 2.
>>>>>>> severalChangesBranch
                                            !If you don't want to get the OptProp curves put hwnum = 1

END MODULE ParamModule
