!   Purpose:
!       This module contains all of the functions used to calculate
!       physical parameters and variables
!   Warning:
!       Don't change anything here

!The following functions the effective mass in GaAS/GaAlAs material under the combined effects of temperature,
    !hydrostatic pressure and Al concentration. Also the dielectric constant as a function of temperature and hydrostatic
    !pressure.
MODULE PhysModule
IMPLICIT NONE
    !-----------------------------------------------------
    !RATIO BETWEEN M*(X,P,T)/M*(0,0,4)

    CONTAINS

    REAL FUNCTION ALPHAMASS(X,P,T)
        REAL :: X,P,T   !Concentration of Al, Manometric presure and Temperature
        ALPHAMASS = EFECMASS(X,P,T)/EFECMASS(0.0,0.0,4.0)
    END FUNCTION ALPHAMASS

    !RATIO BETWEEN M*(X,P,T)/MO MO:FREE ELECTRON MASS AT REST
    REAL FUNCTION EFECMASS(X,P,T)

        REAL :: X,P,T   !Concentration of Al, Manometric presure and Temperature
        EFECMASS= ( 1 + PI2(X)/3*(2/EGAP(X,P,T) &
        + 1/(EGAP(X,P,T)+DELTAZERO(X))) +DELTA(X) )**(-1.0)

    END FUNCTION

    !RATIO BETWEEN MH*(X,P,T)/MO MO:FREE ELECTRON MASS AT REST
    REAL FUNCTION EFECHOLEMASS(X)

        REAL :: X   !Concentration of Al, Manometric presure and Temperature
        EFECHOLEMASS = 0.082+0.068*X

    END FUNCTION

    !VALENCE BAND SPIN-ORBIT SPLITTING
    REAL FUNCTION DELTAZERO(X)
        REAL :: X       !Concentration of Al
        DELTAZERO=341-66*X
    END FUNCTION

    !PARAMETER OF REMOTE BAND EFFECTS
    REAL FUNCTION DELTA(X)
        REAL :: X       !Concentration of Al
        DELTA=-3.935+0.488*X+4.938*X**2
    END FUNCTION

    !ENERGY GAP FUNCTION
    REAL FUNCTION EGAP(X,P,T)
        REAL :: X,P,T   !Concentration of Al, Manometric presure and Temperature
        REAL :: AI, BI, CI, ALPHAI, BETAI, GAMMAI

        IF(X > 0.45) THEN
            AI=1981
            BI=207
            CI=55
            ALPHAI=-1.35
            BETAI=0.46
            GAMMAI=204
        ELSE
            AI=1519
            BI=1360
            CI=220
            ALPHAI=10.7
            BETAI=0.5405
            GAMMAI=204
        ENDIF

        EGAP=AI+BI*X+CI*X**2+ALPHAI*P-(BETAI*T**2)/(GAMMAI+T)

    END FUNCTION

    !INTERBAND  MATRIX ELEMENT
    REAL FUNCTION PI2(X)
        REAL :: X       !Concentration of Al
        PI2=28900-6290*X
    END FUNCTION

    !STATIC DIELECTRIC CONSTANT
    REAL FUNCTION DIELEC(P,T)
        REAL :: P, T    !Manometric presure and temperature
        DIELEC=12.74*EXP(-16.7*P*1E-4+(9.4*1E-5)*(T-75.6))
    END FUNCTION

    !RADIUS SCALE FACTOR
    REAL FUNCTION ETA(P)
        REAL :: P       !Manometric presure
        REAL :: S11,S12
        S11=0.0016
        S12=-0.00037
        ETA=SQRT(1.0-(S11+2*S12)*P)
    END FUNCTION

    !GAALAS/GAAS STATIC DIELECTRIC CONSTANT RATIO
    REAL FUNCTION BETA(P,T)
        REAL :: P, T    !Manometric presure and temperature
        BETA=DIELEC(P,T)/DIELEC(0E0,4E0)
    END FUNCTION

END MODULE PhysModule
