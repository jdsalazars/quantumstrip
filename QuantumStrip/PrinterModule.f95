!   Purpose:
!       This module contains all of the subroutines used to write
!       arrays and values on text files
!   Warning:
!       Don't change anything here

MODULE PrinterModule
USE ParamModule
IMPLICIT NONE

    !Interface used to print the value of a matrix in a text file
    INTERFACE MatrixWriter

        MODULE PROCEDURE :: cMatrixWriter
        MODULE PROCEDURE :: dMatrixWriter
        MODULE PROCEDURE :: rMatrixWriter

    END INTERFACE MatrixWriter

    CONTAINS
    !-----------------------------------------------------

    !The following subroutines print matrixes and vectors in a text file

    !-----------------------------------------------------
    !Prints a matrix in a text file

    SUBROUTINE cMatrixWriter(Matrix, NRows, NColumns, filename)

        INTEGER, INTENT(IN) :: NRows, NColumns  !Number of rows and columns of the matrix
        CHARACTER(len=20), INTENT(IN) :: filename   !Name of the file
        COMPLEX*16, DIMENSION(NRows, NColumns), INTENT(IN) :: Matrix  !Matrix to be printed
        INTEGER :: i,j

        !Opens an unit to write in
        OPEN(UNIT=1, FILE=filename, ACTION='WRITE')
        !Writes the matrix elements in the file
        DO j=1, NRows
            DO i=1, NColumns
                !Writes the eigenvectors in the UNIT 1
                WRITE(1,1000,advance='no') Matrix(j,i)
1000            FORMAT(' ', 2F20.13)
            END DO
            WRITE(1,'()')
        END DO

        CLOSE(UNIT=1)

    END SUBROUTINE cMatrixWriter
    SUBROUTINE dMatrixWriter(Matrix, NRows, NColumns, filename)

        INTEGER, INTENT(IN) :: NRows, NColumns  !Number of rows and columns of the matrix
        CHARACTER(len=20), INTENT(IN) :: filename   !Name of the file
        DOUBLE PRECISION, DIMENSION(NRows, NColumns), INTENT(IN) :: Matrix  !Matrix to be printed
        INTEGER :: i,j

        !Opens an unit to write in
        OPEN(UNIT=1, FILE=filename, ACTION='WRITE')
        !Writes the matrix elements in the file
        DO j=1, NRows
            DO i=1, NColumns
                !Writes the eigenvectors in the UNIT 1
                WRITE(1,1030,advance='no') Matrix(j,i)
1030            FORMAT(' ', F30.15)
            END DO
            WRITE(1,'()')
        END DO

        CLOSE(UNIT=1)

    END SUBROUTINE dMatrixWriter
    SUBROUTINE rMatrixWriter(Matrix, NRows, NColumns, filename)

        INTEGER, INTENT(IN) :: NRows, NColumns  !Number of rows and columns of the matrix
        CHARACTER(len=20), INTENT(IN) :: filename   !Name of the file
        REAL, DIMENSION(NRows, NColumns), INTENT(IN) :: Matrix  !Matrix to be printed
        INTEGER :: i,j

        !Opens an unit to write in
        OPEN(UNIT=1, FILE=filename, ACTION='WRITE')
        !Writes the matrix elements in the file
        DO j=1, NRows
            DO i=1, NColumns
                !Writes the eigenvectors in the UNIT 1
                WRITE(1,1020,advance='no') Matrix(j,i)
1020            FORMAT(' ', F20.13)
            END DO
            WRITE(1,'()')
        END DO

        CLOSE(UNIT=1)

    END SUBROUTINE rMatrixWriter
    !-----------------------------------------------------


END MODULE PrinterModule
