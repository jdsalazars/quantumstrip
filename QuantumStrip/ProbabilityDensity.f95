!   Purpose:
!       This module contains all the functions, subroutines and parameters needed
!       to calculate the optical properties of the system
!   Warning:
!       Don't change anything here

MODULE ProbabilityDensity

USE ParamModule
USE mathModule

IMPLICIT NONE

    INTEGER, PARAMETER :: numPoints=1000            !Number of points to graph

    CONTAINS

    !This function computes the points in the array to graph the probability density of
    !state i
    FUNCTION DensiProb(EigenVectors,i)

        COMPLEX*16, DIMENSION(2*ADIM+1,2*ADIM+1) :: EigenVectors    !Matrix of all the vectors
        DOUBLE PRECISION, DIMENSION(numPoints,3) :: DensiProb         !Probability density
        DOUBLE PRECISION :: phi                     !Angle to work
        COMPLEX*16, DIMENSION(numPoints) :: ComplexDensiProb
        INTEGER :: i            !State to be graphed
        INTEGER :: j,k            !counters

        DensiProb = 0
        ComplexDensiProb = 0

        IF (i > 0) THEN
            DO j = 1, 2*ADIM+1
                DO k = 1, numPoints

                    phi = -pi + 2*pi*(k-1)/(numPoints-1)

                    ComplexDensiProb(k) = ComplexDensiProb(k) &
                                          + EigenVectors(j,i)*EXP(CMPLX(0,1)*phi*(j-ADIM-1))

                    DensiProb(k,1) = phi

                    DensiProb(k,3) = heightFunction(phi)

                ENDDO
            ENDDO
        ELSE
            WRITE(*,*)"Error.  The state must be a positive integer"
        ENDIF

        DensiProb(1:numPoints,2) = REAL(ComplexDensiProb*CONJG(ComplexDensiProb))/(2*pi)

    END FUNCTION DensiProb

END MODULE ProbabilityDensity
