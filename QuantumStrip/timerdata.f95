MODULE timerdata
IMPLICIT NONE
SAVE
!BE SURE OF CHANGING THE PARAMETER P WHEN INSERTING THIS MODULE IN THE PROGRAM


    INTEGER :: o = 1    !Increases 1 every time that the subroutine is used
    INTEGER, PARAMETER :: p = 2 !Number of calls of subroutine time_marker
    INTEGER, DIMENSION(5*p) :: q    !Time marker array

    CONTAINS

    SUBROUTINE time_marker
    IMPLICIT NONE

        INTEGER, DIMENSION(8) :: val        !Contains the "value" out variable of date_and_time subroutine

        CALL DATE_AND_TIME(VALUES=val)
        q(o) = val(3)
        q(o+1) = val(5)
        q(o+2) = val(6)
        q(o+3) = val(7)
        q(o+4) = val(8)
        o = o + 5

    END SUBROUTINE time_marker

    SUBROUTINE time_elapsed
    IMPLICIT NONE

        INTEGER :: j                        !Counter
        INTEGER :: time                     !Time elapsed between procedures
        INTEGER :: total = 0                !Total time elapsed

        DO j = 1, 5*(p-1) , 5

            time = (q(j+5)-q(j))*86400000+(q(j+6)-q(j+1))*3600000+(q(j+7)-q(j+2))*60000+(q(j+8)-q(j+3))*1000+q(j+9)-q(j+4)
            total = total + time
            q(j) = CEILING(time/3600000.)-1
            time = time - q(j)*3600000
            q(j+1) = CEILING(time/60000.)-1
            time = time - q(j+1)*60000
            q(j+2) = CEILING(time/1000.)-1
            q(j+3) = time - q(j+2)*1000

            WRITE(*,100) ((j+4)/5), ((j+4)/5)+1, q(j), q(j+1), q(j+2), q(j+3)
100         FORMAT(' ', 'The time elapsed between the procedure ', I2, ' and ', I2, ' is: ', /, &
I2, ' Hours', 3X, I2, ' Minutes', 3X, I2, ' Seconds', 3X, I3, ' Miliseconds')

        END DO

        q(1) = CEILING(total/3600000.)-1
        total = total - q(1)*3600000
        q(2) = CEILING(total/60000.)-1
        total = total - q(2)*60000
        q(3) = CEILING(total/1000.)-1
        q(4) = total - q(3)*1000

        WRITE(*,101) q(1), q(2), q(3), q(4)
101     FORMAT(' ', 'The total time elapsed is:', I2, ' Hours', 3X, I2, ' Minutes', 3X, I2, ' Seconds', 3X, I3, ' Miliseconds')

    END SUBROUTINE time_elapsed

END MODULE timerdata

